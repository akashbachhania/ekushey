    <!-- END OVERLAY -->
    <!-- BEGIN VENDOR JS -->
    <script src="<?php echo base_url();?>assets/assets/plugins/pace/pace.min.js" type="text/javascript"></script>

   <!-- <script src="<?php //echo base_url();?>assets/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
-->
    <script src="<?php echo base_url();?>assets/assets/plugins/modernizr.custom.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/jquery/jquery-easy.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/jquery-unveil/jquery.unveil.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/jquery-bez/jquery.bez.min.js"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/jquery-ios-list/jquery.ioslist.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/jquery-actual/jquery.actual.min.js"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/jquery-scrollbar/jquery.scrollbar.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/assets/plugins/bootstrap-select2/select2.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/assets/plugins/classie/classie.js"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/switchery/js/switchery.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/nvd3/src/models/line.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/nvd3/src/models/lineWithFocusChart.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/boostrapv3/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url();?>assets/assets/plugins/rickshaw/rickshaw.js"></script>

    <script src="<?php echo base_url();?>assets/assets/plugins/mapplic/js/jquery.mousewheel.js"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/mapplic/js/mapplic.js"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/rickshaw/rickshaw.js"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/jquery-metrojs/MetroJs.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/jquery-sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/skycons/skycons.js" type="text/javascript"></script>
    <script src="<?php echo base_url();?>assets/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js" type="text/javascript"></script>
    <!-- END VENDOR JS -->
<!-- temporary js -->
    <script type="text/javascript" src="<?php echo base_url()?>assets/js/custom.js"></script>
<!-- temporary js -->
    <!-- BEGIN PAGE LEVEL JS -->
    <?php 

        if( $this->uri->segment(2) != 'images' ){ ?>
            <script src="<?php echo base_url();?>assets/assets/js/dashboard.js" type="text/javascript"></script>
        <?php }


     ?>

    <!-- END PAGE LEVEL JS -->
    <script src="<?php echo base_url()?>assets/assets/plugins/interactjs/interact.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/assets/plugins/moment/moment-with-locales.min.js"></script>
    <!-- END VENDOR JS -->
    <!-- BEGIN CORE TEMPLATE JS -->
    <script src="<?php echo base_url()?>assets/pages/js/pages.min.js"></script>
    <script src="<?php echo base_url()?>assets/pages/js/pages.calendar.min.js"></script>
    <!-- END CORE TEMPLATE JS -->
    <!-- BEGIN PAGE LEVEL JS -->
    <script src="<?php echo base_url()?>assets/assets/js/calendar_month.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/assets/js/scripts.js" type="text/javascript"></script>
    <!-- END PAGE LEVEL JS -->

  
    <script src="<?php echo base_url()?>assets/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/assets/plugins/jquery-datatable/extensions/TableTools/js/dataTables.tableTools.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/assets/plugins/jquery-datatable/media/js/dataTables.bootstrap.js" type="text/javascript"></script>
    <script src="<?php echo base_url()?>assets/assets/plugins/jquery-datatable/extensions/Bootstrap/jquery-datatable-bootstrap.js" type="text/javascript"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/assets/plugins/datatables-responsive/js/datatables.responsive.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>assets/assets/plugins/datatables-responsive/js/lodash.min.js"></script>
    <script src="<?php echo base_url()?>assets/assets/js/datatables.js" type="text/javascript"></script>

    <!-- script added from old site -->
    <script src="<?php echo base_url()?>assets/js/jquery.multi-select.js"></script>
    <script src="assets/js/selectboxit/jquery.selectBoxIt.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/fileinput.js"></script>
    <script src="<?php echo base_url()?>assets/js/toastr.js"></script>


    <script src="<?php echo base_url()?>assets/js/jquery.validate.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/jquery.form.js"></script>
    <?php 
            
        if( $this->uri->segment(2) == 'facebook' ){ ?>

            <script src="<?php echo base_url()?>assets/assets/plugins/jquery-isotope/isotope.pkgd.min.js" type="text/javascript"></script>
            <script type="text/javascript" src="<?php echo base_url()?>assets/pages/js/pages.social.js"></script>';
        
    <?php } 

        if( $this->uri->segment(2) == 'images' ){ ?>

<script src="<?php echo base_url()?>assets/assets/plugins/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="<?php echo base_url()?>assets/assets/plugins/jquery-isotope/isotope.pkgd.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/assets/plugins/codrops-dialogFx/dialogFx.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/assets/plugins/owl-carousel/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/assets/plugins/jquery-nouislider/jquery.nouislider.min.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/assets/plugins/jquery-nouislider/jquery.liblink.js" type="text/javascript"></script>
 
 
<script src="<?php echo base_url()?>assets/pages/js/pages.min.js"></script>
 
 
<script src="<?php echo base_url()?>assets/assets/js/gallery.js" type="text/javascript"></script>


    <?php } ?>
   
    <!-- script added from old site -->



    <script src="<?php echo base_url()?>assets/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo base_url()?>assets/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.js"></script>
    <!-- END VENDOR JS -->


<<<<<<< HEAD
=======



<!--    <script src="<?php //echo base_url();?>assets/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>-->
>>>>>>> 1277673acd60e77b78507f87ffb267c5eb21d8b5
