    <script type="text/javascript">
        //$("#tableWithDynamicRows").datatables();
    </script>
    <link rel="apple-touch-icon" href="<?php echo base_url();?>assets/pages/ico/60.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?php echo base_url();?>assets/pages/ico/76.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?php echo base_url();?>assets/pages/ico/120.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?php echo base_url();?>assets/pages/ico/152.png">
    <link rel="icon" type="image/x-icon" href="favicon.ico" />
    <link href="<?php echo base_url();?>assets/assets/plugins/pace/pace-theme-flash.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/assets/plugins/boostrapv3/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/assets/plugins/font-awesome/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/assets/plugins/jquery-scrollbar/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url();?>assets/assets/plugins/bootstrap-select2/select2.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url();?>assets/assets/plugins/switchery/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url();?>assets/assets/plugins/nvd3/nv.d3.min.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url();?>assets/assets/plugins/mapplic/css/mapplic.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/assets/plugins/rickshaw/rickshaw.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url();?>assets/assets/plugins/bootstrap-datepicker/css/datepicker3.css" rel="stylesheet" type="text/css" media="screen">
    <link href="<?php echo base_url();?>assets/assets/plugins/jquery-metrojs/MetroJs.css" rel="stylesheet" type="text/css" media="screen" />
    <link href="<?php echo base_url();?>assets/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
    <link class="main-stylesheet" href="<?php echo base_url();?>assets/pages/css/pages.css" rel="stylesheet" type="text/css" />
    <link class="main-stylesheet" href="<?php echo base_url();?>assets/pages/css/myfont.css" rel="stylesheet" type="text/css" />

<!---->
<script src="<?php echo base_url()?>assets/js/amcharts/amcharts.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/amcharts/pie.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/amcharts/serial.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/amcharts/gauge.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/amcharts/funnel.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/amcharts/radar.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/amcharts/exporting/amexport.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/amcharts/exporting/rgbcolor.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/amcharts/exporting/canvg.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/amcharts/exporting/jspdf.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/amcharts/exporting/filesaver.js" type="text/javascript"></script>
<script src="<?php echo base_url()?>assets/js/amcharts/exporting/jspdf.plugin.addimage.js" type="text/javascript"></script>




<!--      <link href="<?php //echo base_url();?>assets/assets/plugins/dropzone/css/dropzone.css" rel="stylesheet" type="text/css" />
     <script type="text/javascript" src="<?php //echo base_url();?>assets/assets/plugins/dropzone/dropzone.min.js"></script>    
 -->
<link rel="stylesheet" href="assets/js/dropzone/dropzone.css">
<script src="assets/js/dropzone/dropzone.js"></script>

    <link href="<?php echo base_url();?>assets/assets/plugins/datatables-responsive/css/datatables.responsive.css" rel="stylesheet" type="text/css" media="screen" />

    <link href="<?php echo base_url();?>assets/assets/plugins/jquery-nouislider/jquery.nouislider.css" rel="stylesheet" type="text/css" media="screen" />

    <script src="<?php echo base_url();?>assets/assets/plugins/jquery/jquery-1.11.1.min.js" type="text/javascript"></script>
 <link rel="stylesheet" href="http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">



<link rel="stylesheet" href="css/neon-forms.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/custom.css">




    <link class="main-stylesheet" href="<?php echo base_url();?>assets/assets/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />

<!--css for calender--><!-- 
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/neon-theme.css"> --> 
    <!-- <link rel="stylesheet" href="<?php echo base_url();?>assets/css/neon-forms.css"> -->
<?php if( $this->uri->segment(2) == 'images' ){ ?>
<link href="<?php echo base_url();?>assets/assets/plugins/codrops-dialogFx/dialog.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url();?>assets/assets/plugins/codrops-dialogFx/dialog-sandra.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url();?>assets/assets/plugins/owl-carousel/assets/owl.carousel.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url();?>assets/assets/plugins/jquery-nouislider/jquery.nouislider.css" rel="stylesheet" type="text/css" media="screen"/>
<link href="<?php echo base_url();?>assets/pages/css/pages-icons.css" rel="stylesheet" type="text/css">
<link class="main-stylesheet" href="<?php echo base_url();?>assets/pages/css/pages.css" rel="stylesheet" type="text/css"/>
<?php } ?>


<!--for report js-->

<!-- <script src="assets/js/jquery-1.11.0.min.js"></script>
<script src="assets/js/gsap/main-gsap.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/joinable.js"></script>
<script src="assets/js/resizeable.js"></script>
<script src="assets/js/neon-api.js"></script>


<script src="assets/js/neon-custom.js"></script>
 -->