<div class="row">

	<div class="col-md-12">
		
		<ul data-init-reponsive-tabs="collapse" role="tablist" class="nav nav-tabs nav-tabs-simple hidden-xs">
        	<li class="active">
				<a href="#running" data-toggle="tab">
					<span><i class="pg-home"></i>
					<?php echo get_phrase('running');?></span>
				</a>
			</li>
			<li class="">
				<a href="#archived" data-toggle="tab">
					<span><i class="fa fa-file-archive-o"></i>
					<?php echo get_phrase('archived');?></span>
				</a>
			</li>
		</ul>
		
		<div class="tab-content">
			<div class="tab-pane active" id="running">
				
				<?php include 'running_project.php';?>
				
			</div>
			<div class="tab-pane" id="archived">
				
				<?php include 'archived_project.php';?>
					
			</div>
		</div>
		
		
	</div>

</div>


