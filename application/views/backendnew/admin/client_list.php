      <table class="table table-hover demo-table-dynamic" id="tableWithDynamicRows">
        <thead>
          <tr>
            <th style="width:30px;"></th>
            <th><?php echo get_phrase('name'); ?></th>
            <th><?php echo get_phrase('address'); ?></th>
            <th><?php echo get_phrase('project'); ?></th>
            <th><?php echo get_phrase('email'); ?></th>
            <th><?php echo get_phrase('skype'); ?></th>
            <th><?php echo get_phrase('phone'); ?></th>
            <th><?php echo get_phrase('company'); ?></th>
            <th><?php echo get_phrase('website'); ?></th>
            <th><?php echo get_phrase('contact'); ?></th>
            <th><?php echo get_phrase('options'); ?></th>
          </tr>
        </thead>
        <tbody>
        <?php
        $counter = 1;
        $this->db->order_by('client_id', 'desc');
        $clients = $this->db->get('client')->result_array();
        foreach ($clients as $row):
            ?>
            <tr>
            <td style="width:30px;">
                <?php echo $counter++; ?>
            </td>
            <td><?php echo $row['name']; ?></td>
            <td><?php echo $row['address']; ?></td>
            <td><?php echo $this->db->get_where('project', array('client_id' => $row['client_id']))->num_rows(); ?></td>
            <td><?php echo $row['email']; ?></td>
            <td><?php echo $row['skype_id']; ?></td>
            <td><?php echo $row['phone']; ?></td>
            <td>
                <?php 
                    $query =  $this->db->get_where('company' , array('client_id' => $row['client_id']));
                    if ($query->num_rows() > 0)
                        echo $query->row()->name;
                ?>
            </td>
            <td><?php echo $row['website']; ?></td>
            <td>
                <?php if ($row['skype_id'] != ''): ?>
                    <a class="tooltip-primary" data-toggle="tooltip" data-placement="top" 
                       data-original-title="<?php echo get_phrase('call_skype'); ?>"    
                       href="skype:<?php echo $row['skype_id']; ?>?chat" style="color:#bbb;">
                        <i class="fa fa-skype"></i>
                    </a>
                <?php endif; ?>
                <?php if ($row['email'] != ''): ?>
                    <a class="tooltip-primary" data-toggle="tooltip" data-placement="top" 
                       data-original-title="<?php echo get_phrase('send_email'); ?>"    
                       href="mailto:<?php echo $row['email']; ?>" style="color:#bbb;">
                        <i class="pg-mail"></i>
                    </a>
                <?php endif; ?>
                <?php if ($row['phone'] != ''): ?>
                    <a class="tooltip-primary" data-toggle="tooltip" data-placement="top" 
                       data-original-title="<?php echo get_phrase('call_phone'); ?>"    
                       href="tel:<?php echo $row['phone']; ?>" style="color:#bbb;">
                        <i class="pg-phone"></i>
                    </a>
                <?php endif; ?>
                <?php if ($row['facebook_profile_link'] != ''): ?>
                    <a class="tooltip-primary" data-toggle="tooltip" data-placement="top" 
                       data-original-title="<?php echo get_phrase('facebook_profile'); ?>"  
                       href="<?php echo $row['facebook_profile_link']; ?>" style="color:#bbb;" target="_blank">
                        <i class="fa fa-facebook"></i>
                    </a>
                <?php endif; ?>
                <?php if ($row['twitter_profile_link'] != ''): ?>
                    <a class="tooltip-primary" data-toggle="tooltip" data-placement="top" 
                       data-original-title="<?php echo get_phrase('twitter_profile'); ?>"   
                       href="<?php echo $row['twitter_profile_link']; ?>" style="color:#bbb;" target="_blank">
                        <i class="fa fa-twitter"></i>
                    </a>
                <?php endif; ?>
                <?php if ($row['linkedin_profile_link'] != ''): ?>
                    <a class="tooltip-primary" data-toggle="tooltip" data-placement="top" 
                       data-original-title="<?php echo get_phrase('linkedin_profile'); ?>"  
                       href="<?php echo $row['linkedin_profile_link']; ?>" style="color:#bbb;" target="_blank">
                        <i class="fa fa-linkedin"></i>
                    </a>
                <?php endif; ?>
                <?php if ($row['website'] != ''): ?>
                    <a class="tooltip-primary" data-toggle="tooltip" data-placement="top" 
                       data-original-title="<?php echo get_phrase('website'); ?>"   
                       href="<?php echo $row['website']; ?>" style="color:#bbb;" target="_blank">
                        <i class="fa fa-magnet"></i>
                    </a>
                <?php endif; ?>
            </td>
            <td>


                <div class="btn-group">
                    <button type="button" class="btn btn-default btn-sm dropdown-toggle " data-toggle="dropdown">
                        Action <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-default pull-right" role="menu">

                        <!-- PROFILE LINK -->
                        <li>
                            <a href="#" onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/client_profile/<?php echo $row['client_id']; ?>');">
                                <i class="fa fa-star-o"></i>
                                <?php echo get_phrase('profile'); ?>
                            </a>
                        </li>

                        <!-- EDITING LINK -->
                        <li>
                            <a onclick="showAjaxModal('<?php echo base_url(); ?>index.php?modal/popup/client_edit/<?php echo $row['client_id']; ?>');">
                                <i class="fa fa-pencil"></i>
                                <?php echo get_phrase('edit'); ?>
                            </a>
                        </li>
                        <li class="divider"></li>

                        <!-- DELETION LINK -->
                        <li>
                            <a href="#" onclick="confirm_modal('<?php echo base_url(); ?>index.php?admin/client/delete/<?php echo $row['client_id']; ?>', '<?php echo base_url(); ?>index.php?admin/reload_client_list');" >
                                <i class="pg-trash"></i>
                                <?php echo get_phrase('delete'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
      </table>
<!-- calling ajax form submission plugin for specific form -->
<script src="assets/js/ajax-form-submission.js"></script>


