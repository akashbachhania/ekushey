<?php 
	$edit_data	=	$this->db->get_where('company' , array(
		'company_id' => $param2
	))->result_array();
	foreach ($edit_data as $row):
?>
<p class="small-text"><?php echo get_phrase('update_company');?></p>

<?php echo form_open(base_url() . 'index.php?admin/company/edit/' . $row['company_id'] , array('class' => 'form-horizontal form-groups-bordered validate ajax-submit', 'enctype' => 'multipart/form-data' , 'id'=>'form-personal'));?>
	<div class="row">
    <div class="col-sm-12">
      <div class="form-group form-group-default required" aria-required="true">
        <label><?php echo get_phrase('name'); ?></label>
        <input type="text" required="" name="name" class="form-control" aria-required="true" placeholder="<?php echo get_phrase('name');?>" value="<?php echo $row['name'];?>">
      </div>
    </div>

</div>

<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('address'); ?></label>
            <input type="text" class="form-control" name="address"  value="<?php echo $row['address'];?>" placeholder="<?php echo get_phrase('address');?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('phone'); ?></label>
            <input type="text" class="form-control" name="phone" value="<?php echo $row['phone'];?>" placeholder="<?php echo get_phrase('phone');?>" >
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('email'); ?></label>
            <input type="text" class="form-control" name="email" value="<?php echo $row['email'];?>" placeholder="<?php echo get_phrase('email');?>" >
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('website'); ?></label>
            <input type="text" class="form-control" name="website" value="<?php echo $row['website'];?>" placeholder="<?php echo get_phrase('website');?>" >
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('associated_person'); ?></label>
             <select name="client_id" class="select2">
                <option><?php echo get_phrase('select_associated_person'); ?></option>
                <?php
                $clients = $this->db->get('client')->result_array();
                foreach ($clients as $row2):
                    ?>
                    <option value="<?php echo $row2['client_id']; ?>" 
                    	<?php if ($row['client_id'] == $row2['client_id'])
                    		echo 'selected';?>>
                        <?php echo $row2['name']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
</div>

<div class="row">

	<div class="form-group">
	    <div class="col-sm-offset-4 col-sm-7">
	        <button type="submit" class="btn btn-primary" id="submit-button"><?php echo get_phrase('update');?></button>
	     
	     <span id="preloader-form"></span>
	    </div>
	</div>
</div>
<?php endforeach;?>

<script>
	// url for refresh data after ajax form submission
	var post_refresh_url	=	'<?php echo base_url();?>index.php?admin/reload_company_list';
	var post_message		=	'Data Upadted Successfully';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="assets/js/ajax-form-submission.js"></script>

<script type="text/javascript">
	// Select2 Dropdown replacement
    if($.isFunction($.fn.select2))
    {
        $(".select2").each(function(i, el)
        {
            var $this = $(el),
                opts = {
                    allowClear: attrDefault($this, 'allowClear', false)
                };
            
            $this.select2(opts);
            $this.addClass('visible');
            
            //$this.select2("open");
        });
    }
</script>
<script type="text/javascript">  
  $('#form-personal').validate();
  //$('input.date').datepicker();
</script>





