      <!-- START PAGE CONTENT WRAPPER -->
      <div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
            <!-- START CATEGORY -->
            <div class="gallery" style="margin:26px auto 0 auto">
              
              <!-- START GALLERY ITEM -->
              <!-- 
                    FOR DEMO PURPOSES, FIRST GALLERY ITEM (.first) IS HIDDEN 
                    FOR SCREENS <920px. PLEASE REMOVE THE CLASS 'first' WHEN YOU IMPLEMENT 
                -->
              <?php for ($i=2; $i < count($images); $i++) { ?>
                
              
              <div class="gallery-item" data-width="1" data-height="1">
                <!-- START PREVIEW -->
                <img src="assets/img/gallery/<?php echo $images[$i]?>" alt="" class="image-responsive-height">
                <!-- END PREVIEW -->
              </div>
              
              <?php } ?>
              <!-- END GALLERY ITEM -->
            </div>
            <!-- END CATEGORY -->
          </div>
          <!-- START DIALOG -->
          <div id="itemDetails" class="dialog item-details">
            <div class="dialog__overlay"></div>
            <div class="dialog__content">
              <div class="container-fluid">
                <div class="row dialog__overview">
                  <div class="col-sm-12 no-padding item-slideshow-wrapper full-height">
                    <div class="item-slideshow full-height">
                      <div class="slide" data-image="" style="width:843px">
                      </div>
                    </div>
                  </div>
                  
                  
                </div>

              </div>
              <button class="close action top-right" data-dialog-close><i class="pg-close fs-14"></i>
              </button>
            </div>
          </div>
          <!-- END DIALOG -->
          <div class="quickview-wrapper" id="filters">
            <div class="padding-40 ">
              <a class="builder-close quickview-toggle pg-close" data-toggle="quickview" data-toggle-element="#filters" href="#"></a>
              <form class="" role="form">
                <h5 class="all-caps font-montserrat fs-12 m-b-20">Advance filters</h5>
                <div class="form-group form-group-default ">
                  <label>Project</label>
                  <input type="email" class="form-control" placeholder="Type of select a label">
                </div>
                <h5 class="all-caps font-montserrat fs-12 m-b-20 m-t-25">Advance filters</h5>
                <div class="radio radio-danger">
                  <input type="radio" checked="checked" value="1" name="filter" id="asc">
                  <label for="asc">Ascending order</label>
                  <br>
                  <input type="radio" value="2" name="filter" id="views">
                  <label for="views">Most viewed</label>
                  <br>
                  <input type="radio" value="3" name="filter" id="cost">
                  <label for="cost">Cost</label>
                  <br>
                  <input type="radio" value="4" name="filter" id="latest">
                  <label for="latest">Latest</label>
                </div>
                <h5 class="all-caps font-montserrat fs-12 m-b-20 m-t-25">Price range</h5>
                <div class="bg-danger m-b-10" id="slider-margin">
                </div>
                <button class="pull-right btn btn-danger btn-cons m-t-40">Apply</button>
              </form>
            </div>
          </div>
        </div>
        <!-- END PAGE CONTENT -->
        <!-- START COPYRIGHT -->
        <!-- START CONTAINER FLUID -->
        <!-- START CONTAINER FLUID -->
        <div class="container-fluid container-fixed-lg footer">
          <div class="copyright sm-text-center">
            <p class="small no-margin pull-left sm-pull-reset">
              <span class="hint-text">Copyright &copy; 2014 </span>
              <span class="font-montserrat">REVOX</span>.
              <span class="hint-text">All rights reserved. </span>
              <span class="sm-block"><a href="#" class="m-l-10 m-r-10">Terms of use</a> | <a href="#" class="m-l-10">Privacy Policy</a></span>
            </p>
            <p class="small no-margin pull-right sm-pull-reset">
              <a href="#">Hand-crafted</a> <span class="hint-text">&amp; Made with Love ®</span>
            </p>
            <div class="clearfix"></div>
          </div>
        </div>
        <!-- END COPYRIGHT -->
      </div>
      <!-- END PAGE CONTENT WRAPPER -->
    </div>
    <!-- END PAGE CONTAINER -->