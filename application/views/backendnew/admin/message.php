<br/><br/>
<div class="container-fluid container-fixed-lg">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">

    	<div class="row">
    		<div class="col-md-2">
    			<div class="col-md-12">
	    			<div class="mail-sidebar-row">
						<a href="<?php echo base_url();?>index.php?admin/message/message_new" class="btn btn-success btn-icon">
							<?php echo get_phrase('new_message');?>
							<i class="fa fa-pencil pull-right"></i>
						</a>
					</div>    				
    			</div>
    			
    			<div class="col-md-12" style="margin-top: 10px;">
						<ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">
                        
					  <?php
						$current_user		=	$this->session->userdata('login_type').'-'.$this->session->userdata('login_user_id');

						$this->db->where('sender' , $current_user);
						$this->db->or_where('reciever' , $current_user);
						$message_threads	=	$this->db->get('message_thread')->result_array();
						
						foreach($message_threads as $row):

							// defining the user to show
							if ($row['sender'] == $current_user)
								$user_to_show	=	explode('-' , $row['reciever']);
							if ($row['reciever'] == $current_user)
								$user_to_show	=	explode('-' , $row['sender']);

							$user_to_show_type		=	$user_to_show[0];
							$user_to_show_id		=	$user_to_show[1];
							$unread_message_number	=	$this->crud_model->count_unread_message_of_thread($row['message_thread_code']);

						?>
						<li class="<?php if (isset($current_message_thread_code) && $current_message_thread_code == $row['message_thread_code'])echo 'active';?>">
							<a href="<?php echo base_url();?>index.php?admin/message/message_read/<?php echo $row['message_thread_code'];?>" style="padding:12px;color: #000!important;">
								<i class="fa fa-dot-circle-o"></i>
								
								<?php echo $this->db->get_where($user_to_show_type , array($user_to_show_type.'_id' => $user_to_show_id))->row()->name;?>

								<span class="badge badge-default pull-right" style="color:#000;"><?php echo $user_to_show_type;?></span>
								
								<?php if ($unread_message_number > 0):?>
									<span class="badge badge-secondary pull-right">
										<?php echo $unread_message_number;?>
									</span>
								<?php endif;?>
							</a>
						</li>
						<?php endforeach;?>
					</ul>    				
    			</div>




    		</div>

    		<div class="col-md-10">
    			<div class="mail-body">
		
					<?php include $message_inner_page_name.'.php';?>
		
				</div>
    		</div>
    	</div>

    </div>
  </div>
</div>