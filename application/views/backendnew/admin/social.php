<div class="page-content-wrapper ">
        <!-- START PAGE CONTENT -->
        <div class="content ">
          <div class="social-wrapper">
            <div class="social " data-pages="social">
              
              <div class="container-fluid container-fixed-lg sm-p-l-20 sm-p-r-20">
                <div class="feed">
                  <!-- START DAY -->
                  <div class="day" data-social="day">

                    <!-- START ITEM -->
                    
                    <?php foreach ($data->data as $feed) { ?>

                    <div class="card share  col1" data-social="item">
                      <div class="circle" data-toggle="tooltip" title="Label" data-container="body">
                      </div>
                      <div class="card-header clearfix">
                        <div class="user-pic">
<!--                           <img alt="Profile Image" width="33" height="33" data-src-retina="assets/img/profiles/8x.jpg" data-src="assets/img/profiles/8.jpg" src="assets/img/profiles/8x.jpg"> -->
                        </div>
                        <h5><?php echo $feed->from->name ?></h5>
                      </div>
                      <div class="card-description">
                        <?php if( isset($feed->story) && ($feed->story != '') ){ 

                        	echo "<p>" . $feed->story . "</p>";	
                        
                         } elseif( isset($feed->message) && ($feed->message != '') ){ 

                         	echo "<p>" . $feed->message . "</p>";

                         }?>

                        <div class="card-content">
                        	<?php 

                        		if( isset($feed->source) && ($feed->source != '') ){
                        			echo '<video width="250" height="240" controls>
											 <source src="'. $feed->source .'" type="video/mp4">
											 Your browser does not support the video tag.
										  </video>';
                        			
                        			if( isset($feed->description) && ($feed->description != '') ){
                        				if( isset($feed->link) && ($feed->link != '') ){
                        				   echo '<a href="'. $feed->link .'" style="color:#000;"><p style="font-size:16px;font-weight:Bold;">'. $feed->description .'</p></a>';
                        				}
                        				else{
                        					echo '<p style="font-size:16px;font-weight:Bold;">'. $feed->description .'</p>';
                        				}
                        			}
                        		}

                        		elseif( isset($feed->picture) && ($feed->picture != '') ) {
                        			
                        			if( isset($feed->link) && ($feed->link != '') ){
                        				echo '<a href="'. $feed->link .'"> <img src="'. $feed->picture .'"/></a>';
                        			}else{
                        				echo '<img src="'. $feed->picture .'"/>';	
                        			}
                        			

                        		}


                        	?>
                        </div>
                      </div>
                      <div class="card-footer clearfix">
						<ul class="reactions">
							
							
							<li>
							
							<?php if( isset($feed->comments) && ( !empty($feed->comments->data) ) ){ ?>
									<a href="#"><?php echo count($feed->comments->data) ?> <i class="fa fa-comment-o"></i></a>
							
							
							<?php } ?>
							
							</li>

							<li>
							
							<?php if( isset($feed->likes) && ( !empty($feed->likes->data) ) ){ ?>
								
									<a href="#"><?php echo count($feed->likes->data) ?> <i class="fa fa-heart-o"></i></a>
								 
							<?php } ?>
							
							</li>
								
							
						</ul>
						<div class="clearfix"></div>
					  </div>
                    </div>

                    <?php } ?>
                    <!-- END ITEM -->
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>