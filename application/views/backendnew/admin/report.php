<script src="assets/js/jquery-1.11.0.min.js"></script>
<script src="assets/js/gsap/main-gsap.js"></script>
<script src="assets/js/bootstrap.js"></script>
<script src="assets/js/joinable.js"></script>
<script src="assets/js/resizeable.js"></script>
<script src="assets/js/neon-api.js"></script>
<script src="assets/js/neon-custom.js"></script>

<?php echo form_open(base_url() . 'index.php?admin/report/' . $report_type , array('class' => 'form-horizontal form-groups validate',  'id' => 'date_selector_form'));?>
     

<!-- REPORT DATE RANGE SELECTOR STARTS-->           
<div class="form-group">

	<div class="col-sm-4 col-sm-offset-4">

		<div class="daterange daterange-inline add-ranges" data-format="D MMMM, YYYY" style="cursor:text;"
			data-start-date="<?php echo date("d F, Y" , $timestamp_start);?>" data-end-date="<?php echo date("d F, Y" , $timestamp_end);?>">
			<i class="fa fa-calendar"></i>
				<span id="date_range_selector" style="font-weight: 300;font-size: 20px;color:#000;">
					<?php echo date("d M, Y" , $timestamp_start) . " - " . date("d M, Y" , $timestamp_end);?>
				</span>
				<input id="date_range" type="hidden" name="date_range" value="<?php echo date("d F, Y" , $timestamp_start) . " - " . date("d F, Y" , $timestamp_end);?>">
		</div>

	</div>
	<label class="col-sm-4" style="text-align:left; padding-top:0px;">
		<button type="button" class="btn btn-primary btn-lg" id="submit-button"
			onclick="update_date_range();">
			<?php echo get_phrase('search');?>
				</button>
	</label>
</div>

<script>
function update_date_range()
{
	var x = $("#date_range_selector").html();
	$("#date_range").val(x);
	$("#date_selector_form").submit();
}
</script>
<!-- REPORT DATE RANGE SELECTOR ENDS-->


<div style="clear:both;"></div>
<br />
<div class="main_data">
	<?php include "report_" . $report_type . "_body.php";?>
</div>



<!-- Include Required Prerequisites -->
<script type="text/javascript" src="//cdn.jsdelivr.net/jquery/1/jquery.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
 
<!-- Include Date Range Picker -->
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<?php echo '<pre>'; print_r($_POST);die;?>
<script type="text/javascript">
$(function() {

    function cb(start, end) {
        $('.daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
        //alert($('.daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY')));
    }
    cb(moment().subtract(29, 'days'), moment());

    $('.daterange').daterangepicker({
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);

});
</script>