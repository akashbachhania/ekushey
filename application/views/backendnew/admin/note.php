<br><br>
<button type="button" class="btn btn-primary pull-right" id="submit-button" 
    onclick="create_note()">
    <i class="fa fa-plus"></i>
    <?php echo get_phrase('create_note'); ?>
</button>
<div class="panel-body no-padding bg-white ">
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-transparent main_data">
 			<?php include 'notes_tab_body.php';?>
        </div>
      </div>
      </div>
  </div>
<script type="text/javascript">
	function create_note() {
		$.ajax({
        	url: '<?php //echo base_url();?>index.php?admin/create_note/',
        	success: function(response)
        	{
            	// reload the notes
            	reload_data('<?php //echo base_url();?>index.php?admin/reload_notes_tab_body/' + response);
	      	}
	    });
	}
</script> 