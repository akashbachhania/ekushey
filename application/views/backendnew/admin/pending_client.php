<div class="container-fluid container-fixed-lg">
  <!-- START PANEL -->
  <div class="" ss="panel panel-transparent">
    <div class="panel-heading">
      
      <div class="pull-right">
        <div class="col-xs-12">
        <a href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/client_add/');" 
            class="btn btn-primary pull-right">
                <i class="fa fa-plus"></i>
                <?php echo get_phrase('add_new_client');?>
            </a>

        </div>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="panel-body">
      <ul class="nav nav-tabs bordered">
        <li class="<?php if ($page_name == 'client') echo 'active';?>">
          <a href="<?php echo base_url();?>index.php?admin/client">
            <span class="visible-xs"><i class="entypo-users"></i></span>
            <span class="hidden-xs"><?php echo get_phrase('clients');?></span>
          </a>
        </li>
        <li class="<?php if ($page_name == 'pending_client') echo 'active';?>">
          <a href="<?php echo base_url();?>index.php?admin/pending_client">
            <span class="visible-xs"><i class="entypo-bell"></i></span>
            <span class="hidden-xs"><?php echo get_phrase('pending_clients');?></span>
          </a>
        </li>
      </ul>
      <div class="tab-pane <?php if ($page_name == 'pending_client') echo 'active';?>" id="">
  <?php include 'pending_client_list.php';?>
          </div>
      </div>
    </div>
</div>
