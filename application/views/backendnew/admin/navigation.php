    <!-- BEGIN SIDEBPANEL-->
    <nav class="page-sidebar" data-pages="sidebar">
      <!-- BEGIN SIDEBAR MENU TOP TRAY CONTENT-->
      <div class="sidebar-overlay-slide from-top" id="appMenu">
        <div class="row">
          <div class="col-xs-6 no-padding">
            <a href="#" class="p-l-40"><img src="<?php echo base_url();?>assets/assets/img/demo/social_app.svg" alt="socail">
            </a>
          </div>
          <div class="col-xs-6 no-padding">
            <a href="#" class="p-l-10"><img src="<?php echo base_url();?>assets/assets/img/demo/email_app.svg" alt="socail">
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-xs-6 m-t-20 no-padding">
            <a href="#" class="p-l-40"><img src="<?php echo base_url();?>assets/assets/img/demo/calendar_app.svg" alt="socail">
            </a>
          </div>
          <div class="col-xs-6 m-t-20 no-padding">
            <a href="#" class="p-l-10"><img src="<?php echo base_url();?>assets/assets/img/demo/add_more.svg" alt="socail">
            </a>
          </div>
        </div>
      </div>
      <!-- END SIDEBAR MENU TOP TRAY CONTENT-->
      <!-- BEGIN SIDEBAR MENU HEADER-->
      <div class="sidebar-header">
        <img src="<?php echo base_url();?>assets/assets/img/logo_white.png" alt="logo" class="brand" data-src="<?php echo base_url();?>assets/assets/img/logo_white.png" data-src-retina="<?php echo base_url();?>assets/assets/img/logo_white_2x.png" width="78" height="22">
        <div class="sidebar-header-controls">
          <button type="button" class="btn btn-xs sidebar-slide-toggle btn-link m-l-20" data-pages-toggle="#appMenu"><i class="fa fa-angle-down fs-16"></i>
          </button>
          <button type="button" class="btn btn-link visible-lg-inline" data-toggle-pin="sidebar"><i class="fa fs-12"></i>
          </button>
        </div>
      </div>
      <!-- END SIDEBAR MENU HEADER-->
      <!-- START SIDEBAR MENU -->
      <div class="sidebar-menu">
        <!-- BEGIN SIDEBAR MENU ITEMS-->
        <ul class="menu-items">
         
          <!-- DASHBOARD -->
          <li class="<?php if ($page_name == 'dashboard') echo 'active'; ?>">
              <a href="<?php echo base_url(); ?>index.php?admin/dashboard">
                  <i class="pg-home"></i>
                  <span><?php echo get_phrase('dashboard'); ?></span>
              </a>
              <span class="bg-success icon-thumbnail"><i class="pg-home"></i></span>
          </li>

          <!-- MANAGE CLIENTS AND COMPANY -->
          <li class="<?php if ($page_name == 'client' ||
                              $page_name == 'pending_client' ||
                              $page_name == 'company')
                              echo 'opened active has-sub';?>">
                              
            <a href="javascript:;">
            <span class="title"><?php echo get_phrase('client'); ?></span>
            <span class="arrow"></span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-trophy"></i></span>
            <ul class="sub-menu">
               <li class="<?php if ($page_name == 'client' ||
                                        $page_name == 'pending_client')
                                            echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/client">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('person'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'company') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/company">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('company'); ?></span>
                    </a>
                </li>

            </ul>
          </li>
        <!-- MANAGE TEAM MEMBERS -->
        <li class="<?php if ($page_name == 'staff' ||
                                $page_name == 'account_role' ||
                                    $page_name == 'admins')
                                        echo 'opened active has-sub';?>">
            <a href="javascript:;">
            <span class="title"><?php echo get_phrase('team'); ?></span>
            <span class=" arrow"></span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-users"></i></span>
            <ul class="sub-menu">
                               <li class="<?php if ($page_name == 'admins') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/admins">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('admin'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'staff') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/staff">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('staff'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'account_role') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/account_role">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('permission'); ?></span>
                    </a>
                </li>
            </ul>
          </li>

        <!-- MANAGE CLIENT PROJECTS -->
      <li class="<?php if ($page_name == 'project_add' ||
                                $page_name == 'project' ||
                                    $page_name == 'project_room' ||
                                        $page_name == 'project_quote')
                                            echo 'opened active has-sub';?>">
              <a href="javascript:;">
            <span class="title"><?php echo get_phrase('client_project'); ?></span>
            <span class=" arrow"></span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-paper-plane"></i></span>
            <ul class="sub-menu">
                <li class="<?php if ($page_name == 'project') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/project">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('project_list'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'project_add') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/project_add">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('create_project'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'project_quote') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/project_quote">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('project_quote'); ?></span>
                    </a>
                </li>
            </ul>
          </li>


        <!-- TEAM TASKS -->
        <li class="<?php if ($page_name == 'team_task' ||
                                $page_name == 'team_task_archived' ||
                                    $page_name == 'team_task_view')
                                        echo 'opened active has-sub';?>">
            <a href="javascript:;">
            <span class="title"><?php echo get_phrase('team_task'); ?></span>
            <span class=" arrow"></span>
            </a>
            <span class="icon-thumbnail"><i class="pg-layouts"></i></span>
            <ul class="sub-menu">
                <li class="<?php if ($page_name == 'team_task') echo 'active'; ?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/team_task">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('running_tasks'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'team_task_archived') echo 'active'; ?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/team_task_archived">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('archived_tasks'); ?></span>
                    </a>
                </li>
            </ul>
          </li>
          <li class="<?php if ($page_name == 'calendar') echo 'active';?>">
              <a href="<?php echo base_url(); ?>index.php?admin/calendar">
                  <i class="pg-calendar"></i>
                  <span><?php echo get_phrase('calendar'); ?></span>
              </a>
          <span class="icon-thumbnail"><i class="pg-calender"></i></span>
          </li>
          
          <!-- MESSAGING -->
          <li class="<?php if ($page_name == 'message') echo 'active';?>">
              <a href="<?php echo base_url(); ?>index.php?admin/message">
                  <!-- <i class="pg-mail"></i> -->
                  <span><?php echo get_phrase('message'); ?></span>
              </a>
          <span class="icon-thumbnail"><i class="pg-mail"></i></span>
          </li>

          <!-- NOTE -->

          <li class="<?php if ($page_name == 'note') echo 'active';?>">
              <a href="<?php echo base_url(); ?>index.php?admin/note">
                  <!-- <i class="pg-note"></i> -->
                  <span><?php echo get_phrase('note'); ?></span>
              </a>
          <span class="icon-thumbnail"><i class="pg-note"></i></span>
          </li>


        <!-- ACCOUNTING -->

        <li class="<?php if ($page_name == 'accounting_client_payment' ||
                                $page_name == 'accounting_expense' ||
                                    $page_name == 'accounting_expense_category')
                                        echo 'opened active has-sub';?>">
            <a href="javascript:;">
            <span class="title"><?php echo get_phrase('accounting'); ?></span>
            <span class=" arrow"></span>
            </a>
            <span class="icon-thumbnail"><i class="pg-credit_card"></i></span>
            <ul class="sub-menu">
                <li class="<?php if ($page_name == 'accounting_client_payment') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/accounting_client_payment">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('client_payment'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'accounting_expense') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/accounting_expense">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('expense_management'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'accounting_expense_category') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/accounting_expense_category">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('expense_category'); ?></span>
                    </a>
                </li>
            </ul>
          </li>

        <!-- REPORT -->

        <li class="<?php if ($page_name == 'report')echo 'opened active has-sub';?>">
            <a href="javascript:;">
            <span class="title"><?php echo get_phrase('report'); ?></span>
            <span class=" arrow"></span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-line-chart"></i></span>
            <ul class="sub-menu">
                <li class="<?php if (isset($report_type) && $report_type == 'project') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/report/project">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('project_report'); ?></span>
                    </a>
                </li>
                <li class="<?php if (isset($report_type) && $report_type == 'client') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/report/client">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('client_report'); ?></span>
                    </a>
                </li>
                <li class="<?php if (isset($report_type) && $report_type == 'expense') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/report/expense">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('expense_report'); ?></span>
                    </a>
                </li>
                <li class="<?php if (isset($report_type) && $report_type == 'income_expense') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/report/income_expense">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('income_expense_comparison'); ?></span>
                    </a>
                </li>
            </ul>
          </li>


        <!-- SUPPORT TICKET -->

        <li class="<?php if ($page_name == 'support_ticket_create' ||
                                $page_name == 'support_ticket' ||
                                    $page_name == 'support_ticket_view')
                                        echo 'opened active has-sub';?>">
            <a href="javascript:;">
            <span class="title"><?php echo get_phrase('client_support'); ?></span>
            <span class=" arrow"></span>
            </a>
            <span class="icon-thumbnail"><i class="fa fa-life-bouy"></i></span>
            <ul class="sub-menu">
                <li class="<?php if ($page_name == 'support_ticket') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/support_ticket">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('ticket_list'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'support_ticket_create') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/support_ticket_create">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('create_ticket'); ?></span>
                    </a>
                </li>
            </ul>
          </li>
        <!-- SETTINGS -->

        <li class="<?php if ($page_name == 'system_settings' ||
                                $page_name == 'manage_language' ||
                                    $page_name == 'email_settings' ||
                                        $page_name == 'payment_settings')
                                            echo 'opened active has-sub';?>">
            <a href="javascript:;">
            <span class="title"><?php echo get_phrase('setting'); ?></span>
            <span class=" arrow"></span>
            </a>
            <span class="icon-thumbnail"><i class="pg-settings_small"></i></span>
            <ul class="sub-menu">
                <li class="<?php if ($page_name == 'system_settings') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/system_settings">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('system_settings'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'email_settings') echo 'active';?>">
                    <a href="<?php echo base_url();?>index.php?admin/email_settings">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('email_settings'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'payment_settings') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/payment_settings">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('payment_settings'); ?></span>
                    </a>
                </li>
                <li class="<?php if ($page_name == 'manage_language') echo 'active';?>">
                    <a href="<?php echo base_url(); ?>index.php?admin/manage_language">
                        <i class="fa fa-circle"></i>
                        <span><?php echo get_phrase('language_settings'); ?></span>
                    </a>
                </li>
            </ul>
          </li>


        </ul>  

        <div class="clearfix"></div>
      </div>
      <!-- END SIDEBAR MENU -->
    </nav>
    <!-- END SIDEBAR -->
    <!-- END SIDEBPANEL-->
    <!-- START PAGE-CONTAINER -->