<p class="small-text"><?php echo get_phrase('account_creation_form');?></p>

<?php echo form_open(base_url() . 'index.php?admin/staff/create/' , array( 'id'=>'form-personal' ,'class' => 'form-horizontal form-groups-bordered validate ajax-submit', 'enctype' => 'multipart/form-data'));?>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default required" aria-required="true">
            <label><?php echo get_phrase('name'); ?></label>
                <input type="text" class="form-control" name="name" required="" aria-required="true" placeholder="<?php echo get_phrase('name');?>">
                                    
            </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('account_role'); ?></label>
              <select class="selectboxit form-control" name="account_role_id">
                  <option><?php echo get_phrase('select_a_role');?></option>
                  <?php 
                      $account_roles		=	$this->db->get('account_role')->result_array();
                      foreach($account_roles as $row2):
                      ?>
                          <option value="<?php echo $row2['account_role_id'];?>"><?php echo $row2['name'];?></option>
                   <?php
                  endforeach;
                  ?>
           	</select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('email'); ?></label>
            <input type="text" class="form-control" name="email" placeholder="<?php echo get_phrase('email');?>" >
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('phone'); ?></label>
            <input type="text" class="form-control" name="phone"  placeholder="<?php echo get_phrase('phone');?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('skype_id'); ?></label>
            <input type="text" class="form-control" name="skype_id"  placeholder="<?php echo get_phrase('skype_id');?>">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('facebook_profile_link'); ?></label>
            <input type="text" class="form-control" name="facebook_profile_link" placeholder="<?php echo get_phrase('facebook_profile_link');?>" >
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('linkedin_profile_link'); ?></label>
            <input type="text" class="form-control" name="linkedin_profile_link"  placeholder="<?php echo get_phrase('linkedin_profile_link');?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('twitter_profile_link'); ?></label>
            <input type="text" class="form-control" name="twitter_profile_link" placeholder="<?php echo get_phrase('twitter_profile_link');?>" >
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('photo'); ?></label>
            <div class="col-sm-7">
				<div class="fileinput fileinput-new" data-provides="fileinput">
					<div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
						<img src="uploads/user.jpg" alt="...">
					</div>
					<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
					<div>
						<span class="btn btn-white btn-file">
							<span class="fileinput-new">Select image</span>
							<span class="fileinput-exists">Change</span>
							<input type="file" name="userfile" accept="image/*">
						</span>
						<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
					</div>
				</div>
			</div>
    	</div>
</div>
<div class="form-group">
    <div class="col-sm-offset-4 col-sm-7">
        <button type="submit" class="btn btn-primary" id="submit-button"><?php echo get_phrase('add_staff');?></button>
     
     <span id="preloader-form"></span>
    </div>
</div>
<div class="row">


<?php echo form_close(); ?>

<script>
	// url for refresh data after ajax form submission
	var post_refresh_url	=	'<?php echo base_url();?>index.php?admin/reload_staff_list';
	var post_message		=	'Data Created Successfully';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="assets/js/ajax-form-submission.js"></script>
<script type="text/javascript">  
  $('#form-personal').validate();
</script>