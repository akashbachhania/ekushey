<br><br>
<div class="container-fluid container-fixed-lg">
  <!-- START PANEL -->
  <div class="panel panel-transparent">
    <div class="panel-heading">
      
      <div class="">
        <div class="col-md-3">
          <a href="javascript:;" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/team_task_add/');" 
              class="btn btn-complete btn-block">
                <i class="fa fa-plus"></i>
                <?php echo get_phrase('add_a_new_team_task');?>
          </a>

          <?php
            $this->db->where('task_status' , 1);
            $this->db->order_by('creation_timestamp', 'desc'); 
            $query  = $this->db->get('team_task');
            if ($query->num_rows() > 0):
              $team_tasks = $query->result_array();
              foreach ($team_tasks as $row):
          ?>
          <a style="text-align: left;margin-top: 10px;" href="<?php echo base_url();?>index.php?admin/team_task_view/<?php echo $row['team_task_id'];?>" 
            class="<?php echo 'btn btn-primary';?> btn-block">
            
            <i class="pg-arrow_lright_line_alt"></i> <?php echo $row['task_title'];?>
          </a>
          <?php 
            endforeach;
              endif;
          ?>

        </div>
          <div class="col-md-9">
            <h3 class="text-center" style="color: #ccc;">
              <?php
                if ($query->num_rows() > 0)
                  echo 'Please select a task first.....';
                if ($query->num_rows() < 1)
                  echo 'No running tasks......';
              ?>
            </h3>
          </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
</div>