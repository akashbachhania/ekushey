
<center>
<h4><?php echo get_phrase('summary_report');?> </h4>
</center>
<table class="table table-bordered demo-table-dynamic" id="tableWithDynamicRows" >
	<thead>
		<tr>
			<th><?php echo get_phrase('date');?></th>
			<th><?php echo get_phrase('project');?></th>
			<th><?php echo get_phrase('amount');?></th>
			<th><?php echo get_phrase('payment_method');?></th>
		</tr>
	</thead>
	<tbody>
		
		<?php 
		$grand_total	=	0;
		$this->db->order_by('timestamp' , 'desc');
		
		$this->db->where('type' , 'income');
		$this->db->where('timestamp >=' , $timestamp_start);
		$this->db->where('timestamp <=' , $timestamp_end);
		$payments	=	$this->db->get('payment')->result_array();
		foreach ($payments as $row):
			$grand_total	+=	$row['amount'];
			?>
			<tr>
				<td><?php echo date("d M, Y" , $row['timestamp']);?></td>
				<td><?php echo $this->db->get_where('project',array('project_code' => $row['project_code']))->row()->title;?></td>
				<td><?php echo $row['amount'];?></td>
				<td><?php echo $row['payment_method'];?></td>
			</tr>

		<?php endforeach;?>
	</tbody>
</table>


<!-- <div class="row">
	<div class="col-sm-4 col-md-offset-4">
		
		<div class="tile-stats tile-white tile-white-primary">
		
			<div class="icon" style="bottom:40px;"><i class="fa fa-chart-bar"></i></div>
			<div class="num" data-start="0" data-end="<?php //echo $grand_total;?>" data-prefix="<?php //echo $currency_symbol;?>" 
				style="font-weight:200;"	data-postfix="" data-duration="1500" data-delay="0">
				0
			</div>
			
			<h3 style="font-weight:200; font-size: 15px;"><?php //echo get_phrase('total_income');?></h3>
		</div>
		
	</div>
</div>
 --><div class="row">
		<div class="col-md-4"></div>
  	<div class="col-md-4 m-b-10">
    <!-- START WIDGET widget_progressTileFlat-->
    <div class="widget-9 panel no-border bg-primary no-margin widget-loader-bar">
      <div class="container-xs-height full-height">
        <div class="row-xs-height">
          <div class="col-xs-height col-top">
            <div class="panel-heading  top-left top-right">
              <div class="panel-title text-black">
                <span class="font-montserrat fs-11 all-caps"><?php echo get_phrase('total_income');?><i class="fa fa-chevron-right"></i>
                                    </span>
              </div>
              <div class="panel-controls">
                <ul>
                  <li><a data-toggle="refresh" class="portlet-refresh text-black" href="#"><i class="portlet-icon portlet-icon-refresh"></i></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="row-xs-height">
          <div class="col-xs-height col-top">
            <div class="p-l-20 p-t-15">
              <h3 class="no-margin p-b-5 text-white">
              	
			<div class="tile-stats tile-white tile-white-primary">
		
				<div class="icon" style="bottom:40px;"><i class="fa fa-chart-bar"></i></div>
				<div class="num" data-start="0" data-end="<?php echo $grand_total;?>" data-prefix="<?php echo $currency_symbol;?>" 
					style="font-weight:200;"	data-postfix="" data-duration="1500" data-delay="0">
					0
				</div>
			
		</div>


              </h3>
            </div>
          </div>
        </div>
        <div class="row-xs-height">
          <div class="col-xs-height col-bottom">
            <div class="progress progress-small m-b-20">
              <!-- START BOOTSTRAP PROGRESS (http://getbootstrap.com/components/#progress) -->
<!--               <div style="width:45%" class="progress-bar progress-bar-white"></div>
 -->              <!-- END BOOTSTRAP PROGRESS -->
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- END WIDGET -->
  </div>
<div class="col-md-4"></div>
</div>
<br>
<script>

var chart = AmCharts.makeChart("bar_chartdiv", {
    "theme": "none",
    "type": "serial",
	"startDuration": 2,
    "dataProvider": [
	<?php
		$this->db->select_sum('amount');
		$this->db->group_by('project_code'); 
		$this->db->order_by('timestamp' , 'desc');
		$this->db->select('timestamp, project_code, payment_method');
		
		$this->db->where('type' , 'income');
		$this->db->where('timestamp >=' , $timestamp_start);
		$this->db->where('timestamp <=' , $timestamp_end);
		$payments	=	$this->db->get('payment')->result_array();
		foreach ($payments as $row):
			?>
				{
                    "project": "<?php echo substr($this->db->get_where('project',array('project_code' => $row['project_code']))->row()->title , 0,20);?>",
                    "amount": <?php echo $row['amount'];?>,
                    "color": "#455064"
                },
	<?php endforeach;?> 
	],
    "graphs": [{
        "balloonText": "[[category]]: <b>[[value]]</b>",
        "colorField": "color",
        "fillAlphas": 1,
        "lineAlpha": 0.1,
        "type": "column",
        "valueField": "amount"
    }],
    "depth3D": 20,
	"angle": 30,
    "chartCursor": {
        "categoryBalloonEnabled": false,
        "cursorAlpha": 0,
        "zoomable": false
    },    
    "categoryField": "project",
    "categoryAxis": {
        "gridPosition": "start",
        "labelRotation": 30
    },
	"pathToImages"	: "<?php echo base_url();?>assets/js/amcharts/images/",
	"amExport": {
					"top": 0,
                    "right": 0,
                    "buttonColor": '#EFEFEF',
                    "buttonRollOverColor":'#DDDDDD',
					"imageFileName"	: "Project Report",
                    "exportPNG":true,
                    "exportJPG":true,
                    "exportPDF":true,
                    "exportSVG":true
	}
});
var chart = AmCharts.makeChart("chartdiv",{
	"type"			: "pie",
	"titleField"	: "project",
	"valueField"	: "amount",
	"innerRadius"	: "40%",
	"angle"			: "15",
	"depth3D"		: 10,
	"pathToImages"	: "<?php echo base_url();?>assets/js/amcharts/images/",
	"amExport": {
					"top": 0,
                    "right": 0,
                    "buttonColor": '#EFEFEF',
                    "buttonRollOverColor":'#DDDDDD',
					"imageFileName"	: "Project Report",
                    "exportPNG":true,
                    "exportJPG":true,
                    "exportPDF":true,
                    "exportSVG":true
	},
	"dataProvider"	: [
		<?php
		foreach ($payments as $row):
				?>
		{
			"project": "<?php echo substr($this->db->get_where('project',array('project_code' => $row['project_code']))->row()->title , 0,20);?>",
			"amount": <?php echo $row['amount'];?>
		},
		<?php endforeach;?>
	]
});
</script>
	

<div class="row">
	<!-- BAR DIAGRAM STARTS-->
   	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="panel-title" style="color:#626262">
					<i class="fa fa-bar-chart-o"></i>
					<?php echo get_phrase('project_income_bar');?> 
				</div>
			</div>
			<div class="panel-body">
				<div id="bar_chartdiv" style="width: 100%; height: 350px;"></div>
			</div>
		</div>
	</div>
</div>
	<!-- BAR DIAGRAM FINISHES-->

<div class="row">
	<!-- AM CHART STARTS-->
   	<div class="col-md-12">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="panel-title" style="color:#626262">
					<i class="fa fa-pie-chart"></i>
					<?php echo get_phrase('project_income_percentage');?> 
				</div>
			</div>
			<div class="panel-body">
				<div id="chartdiv" style="width:100%; height:350px;"></div>
			</div>
		</div>
	</div>
	<!-- AM CHART FINISHES-->
</div>







<script src="<?php echo base_url()?>assets/js/neon-custom.js"></script>

<script type="text/javascript">


	
/*	jQuery(document).ready(function($)
	{
		//convert all checkboxes before converting datatable
		replaceCheckboxes();
		var datatable = $("#table_export").dataTable({
			"sPaginationType": "bootstrap",
			"sDom": "<'row'<'col-xs-3 col-left'l><'col-xs-9 col-right'<'export-data'T>f>r>t<'row'<'col-xs-3 col-left'i><'col-xs-9 col-right'p>>",
			
			"aoColumns": [
				{ "bSortable": false}, 
				{ "bSortable": true}, 	
				{ "bVisible": true},		
				{ "bVisible": true},		
				{ "bVisible": true},		
			],
			
		});
		// Highlighted rows
		$("#table_export tbody input[type=checkbox]").each(function(i, el)
		{
			var $this = $(el),
				$p = $this.closest('tr');
			
			$(el).on('change', function()
			{
				var is_checked = $this.is(':checked');
				
				$p[is_checked ? 'addClass' : 'removeClass']('highlight');
			});
		});
		
		//customize the select menu 
		$(".dataTables_wrapper select").select2({
			minimumResultsForSearch: -1
		});
		
	});
*/	
</script>

