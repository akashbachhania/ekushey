<br><br>


         <ul id="tab-3" class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white col-lg-3">
			<?php 
			$email_templates	=	$this->db->get('email_template')->result_array();
			foreach ($email_templates as $row):
			?>
			<li class="<?php if ($row['email_template_id'] == $current_email_template_id)echo 'active';?>">
				<a href="#tab<?php echo $row['email_template_id'];?>"
					data-toggle="tab" data-toggle="tab" aria-expanded="false">
					<?php echo get_phrase($row['task']);?>
					<i class="fa fa-arrow-circle-right"></i>
				</a>
			</li>
			<?php endforeach;?>
			</ul>


  <div class="tab-content bg-white">

				<?php
				foreach ($email_templates as $row):
					?>


			<div class="tab-pane <?php if ($row['email_template_id'] == $current_email_template_id)echo 'active';?>" 
						id="tab<?php echo $row['email_template_id'];?>">
						<?php echo form_open(base_url() . 'index.php?admin/email_settings/do_update/' . $row['email_template_id']);?>
					


			<div class="row column-seperation">
            <div class="row">
                <div class="col-md-12">
                <label><?php echo get_phrase('email_subject');?> :</label>
				<input type="text" class="form-control" name="subject" value="<?php echo $row['subject'];?>">                </div>
            </div>
            <div class="row">
            <div class="col-md-12">
                <label><?php echo get_phrase('email_body');?> :</label>
		      	<textarea class="form-control autogrow" rows="10" name="body" id="post_content<?php echo $row['email_template_id'];?>" 
	           data-stylesheet-url="assets/css/wysihtml5-color.css"><?php echo $row['body'];?></textarea>
      		</div>
            </div>
            <div class="row">
            </div>
            <div class="row">
                <div class="col-md-12">
                <label><?php echo get_phrase('instruction');?> :</label>
				<?php echo $row['instruction'];?>
                </div>
            </div>
            <br/>
            <center>
                <button type="submit" class="btn btn-primary btn-icon icon-left">
					
					<i class="pg-save"></i>
					<?php echo get_phrase('save_template');?>
				</button>
			</center>

        <?php echo form_close();?>

        </div>
    </div>
    <?php endforeach;?>
  </div>
