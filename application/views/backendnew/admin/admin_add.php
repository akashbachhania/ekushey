<p class="small-text">
    <?php echo get_phrase('create_new_admin'); ?>
</p>
<?php echo form_open(base_url() . 'index.php?admin/admins/create/', array('id'=>'form-personal','class' => 'form-horizontal form-groups-bordered validate ajax-submit', 'enctype' => 'multipart/form-data' )); ?>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default required" aria-required="true">
            <label><?php echo get_phrase('name'); ?></label>
            <input type="text"  name="name" class="form-control" required="" aria-required="true" placeholder="<?php echo get_phrase('name');?>" >
            </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('email'); ?></label>
            <input type="text" class="form-control" name="email" placeholder="<?php echo get_phrase('email');?>" >
        </div>
    </div>
</div>
 <div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('password'); ?></label>
            <input type="text" class="form-control" name="password" placeholder="<?php echo get_phrase('password');?>" >
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('address'); ?></label>
            <input type="text" class="form-control" name="address" placeholder="<?php echo get_phrase('address');?>" >
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('phone'); ?></label>
            <input type="text" class="form-control" name="phone" placeholder="<?php echo get_phrase('phone');?>" >
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('admin_type'); ?></label>
	            <select class="form-control selectboxit" name="owner_status">
	                <option value="0"><?php echo get_phrase('administrator');?></option>
	                <option value="1"><?php echo get_phrase('owner');?></option>
	            </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('photo'); ?></label>
			<div class="fileinput fileinput-new" data-provides="fileinput">
					<div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
						<img src="uploads/user.jpg" alt="...">
					</div>
					<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
					<div>
						<span class="btn btn-white btn-file">
							<span class="fileinput-new">Select image</span>
							<span class="fileinput-exists">Change</span>
							<input type="file" name="userfile" accept="image/*">
						</span>
						<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
					</div>
				</div>
				
        </div>
    </div>
</div>
<div class="form-group">
	<div class="col-sm-offset-4 col-sm-7">
		<button type="submit" class="btn btn-primary" id="submit-button"><?php echo get_phrase('add_admin');?></button>
     <span id="preloader-form"></span>
	</div>
</div>
<div class="row">


<?php echo form_close(); ?>




<script>
	// url for refresh data after ajax form submission
	var post_refresh_url	=	'<?php echo base_url();?>index.php?admin/reload_admin_list';
	var post_message		=	'Data Created Successfully';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="assets/js/ajax-form-submission.js"></script>
<script type="text/javascript">  
  $('#form-personal').validate();
  </script>