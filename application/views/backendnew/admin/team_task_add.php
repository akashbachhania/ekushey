<h5 class="">
    <i class="pg-plus_circle"></i>
    <?php echo get_phrase('add_new_team_task'); ?>
</h5>

<?php echo form_open(base_url() . 'index.php?admin/team_task/create/', array('class' => 'form-horizontal form-groups-bordered validate ajax-submit', 'enctype' => 'multipart/form-data','role'=>'form','novalidate'=>"novalidate","id"=>"form-personal")); ?>
<div class="row">
    <div class="col-sm-12">
        <div class="form-group form-group-default required" aria-required="true">
            <label><?php echo get_phrase('task_title');?></label>
                <input type="text" class="form-control" name="task_title" value="" required="" aria-required="true" autofocus>
            </div>
    </div>
</div>
<div class="row">
	<div class="col-sm-12">
		<div pg-form-group class="form-group form-group-default input-group">
			<label><?php echo get_phrase('creation_date');?></label>
			
			<input type="text" class="form-control datepicker date" name="creation_timestamp">
			
			<span class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</span>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div pg-form-group class="form-group form-group-default input-group">
			<label><?php echo get_phrase('due_date');?></label>
			
			<input type="text" class="form-control datepicker date" name="due_timestamp">
			
			<span class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</span>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<label><?php echo get_phrase('assign_staff');?></label>
			
			<select multiple="multiple" name="assigned_staff_ids[]" class="form-control multi-select">
                <?php 
                        $staffs     =   $this->db->get('staff')->result_array();
                        foreach ($staffs as $row):
                        ?>
                        <option value="<?php echo $row['staff_id'];?>"><?php echo $row['name'];?></option>
                    <?php endforeach;?>
            </select>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<label><?php echo get_phrase('task_status');?></label>
			<select class="form-control form-group-default" name="task_status">
				<option value="1"><?php echo get_phrase('running');?></option>
                <option value="0"><?php echo get_phrase('archived');?></option>
            </select>
		</div>
	</div>
</div>

<div class="row">
	<div class="col-sm-12">
		<div class="form-group">
			<button type="submit" class="btn btn-success pull-right" id="submit-button"><?php echo get_phrase('add_team_task'); ?></button>
		</div>
	</div>
</div>

<?php echo form_close(); ?>
<script type="text/javascript">  
  $('#form-personal').validate();
  $('input.date').datepicker();
</script>