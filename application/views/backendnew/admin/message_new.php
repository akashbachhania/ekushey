<div class="panel panel-default">
	<div class="panel-heading">
		<div class="panel-title">
			<h3><?php echo get_phrase('write_new_message');?></h3>
		</div>
	</div>
	<div class="panel-body">
		<?php echo form_open(base_url() . 'index.php?admin/message/send_new/' , array(
		'class' => 'form', 'onsubmit' => 'return check_receiver()' , 'enctype' => 'multipart/form-data'));?>
			<div class="form-group form-group-default form-group-default-select2 required">
		
			    <label class=""><?php echo get_phrase('recipient');?></label>
			    
			    <select class="full-width" name="reciever" id="receiver" data-placeholder="Select Country" data-init-plugin="select2">
			      <option><?php echo get_phrase('select_a_user');?></option>
			      <optgroup label="<?php echo get_phrase('staff');?>">
					<?php 
					$staffs	=	$this->db->get('staff')->result_array();
					foreach ($staffs as $row):
						?>
						<option value="staff-<?php echo $row['staff_id'];?>">
							- <?php echo $row['name'];?></option>

					<?php endforeach;?>
			      </optgroup>
				  <optgroup label="<?php echo get_phrase('client');?>">
					<?php 
					$clients	=	$this->db->get('client')->result_array();
					foreach ($clients as $row):
						?>

						<option value="client-<?php echo $row['client_id'];?>">
							- <?php echo $row['name'];?></option>

					<?php endforeach;?>
				</optgroup>
			    </select>
		    
		    </div>
		    <div class="form-group form-group-default">
			    <div class="wysiwyg5-wrapper email-body-wrapper">
	              <textarea class="wysiwyg email-body"   name="message" 
				placeholder="<?php echo get_phrase('write_your_message');?>"></textarea>
	            </div>
	        </div>

	        <button type="submit" class="btn btn-success btn-icon pull-right">
				<?php echo get_phrase('send');?>
				<i class="pg-mail"></i>
			</button>

		<?php echo form_close();?>
	</div>
</div>

<script type="text/javascript">
	function check_receiver() {
		var check_receiver = $('#receiver').val();
		if (check_receiver == '' || check_receiver == 0) {
			toastr.error("Please select a receiver", "Error");
            return false;
		}
	}
</script>