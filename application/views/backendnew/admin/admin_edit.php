<?php $edit_data	=	$this->db->get_where('admin' , array('admin_id' => $param2))->result_array();
foreach ($edit_data as $row):
?>
<p class="small-text"><?php echo get_phrase('update_admin_data');?></p>

<?php echo form_open(base_url() . 'index.php?admin/admins/edit/'.$row['admin_id'], array('id' => 'form-personal','class' => 'form-horizontal form-groups-bordered validate ajax-submit', 'enctype' => 'multipart/form-data'));?>

<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default required" aria-required="true">
            <label><?php echo get_phrase('name'); ?></label>
                <input type="text" required="" name="name" class="form-control" aria-required="true" data-message-required="<?php echo get_phrase('value_required');?>" 
                                    value="<?php echo $row['name'];?>">
            </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('email'); ?></label>
            <input type="text" class="form-control" name="email" value="<?php echo $row['email'];?>" >
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('address'); ?></label>
            <input type="text" class="form-control" name="address" value="<?php echo $row['address'];?>" >
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('phone'); ?></label>
            <input type="text" class="form-control" name="phone" value="<?php echo $row['phone'];?>" >
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('admin_type'); ?></label>
                <select class="form-control selectboxit" name="owner_status">
	                <option value="0" <?php if ($row['owner_status'] == 0) echo 'selected';?>><?php echo get_phrase('administrator');?></option>
	                <option value="1" <?php if ($row['owner_status'] == 1) echo 'selected';?>><?php echo get_phrase('owner');?></option>
                </select>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('photo'); ?></label>
            <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
                        <img src="<?php echo $this->crud_model->get_image_url('admin' , $row['admin_id']);?>" alt="...">
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
                    <div>
                        <span class="btn btn-white btn-file">
                            <span class="fileinput-new">Select image</span>
                            <span class="fileinput-exists">Change</span>
                            <input type="file" name="userfile" accept="image/*">
                        </span>
                        <a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
                    </div>
                </div>
                
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-4 col-sm-7">
        <button type="submit" class="btn btn-primary" id="submit-button"><?php echo get_phrase('edit_admin');?></button>
     <span id="preloader-form"></span>
    </div>
</div>
<div class="row">


<?php echo form_close(); ?>

<?php endforeach;?>
<script>
    // url for refresh data after ajax form submission
    var post_refresh_url    =   '<?php echo base_url();?>index.php?admin/reload_admin_list';
    var post_message        =   'Data Updated Successfully';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="assets/js/ajax-form-submission.js"></script>
<script type="text/javascript">
    function validate() {
        var search_string = $('#search_input').val();
        var search_string_length = search_string.length;
        if (search_string_length < 2) {
            toastr.error("Please enter minimum 2 characters", "Error");
            return false;
        }
    }
</script> 
<script type="text/javascript">  
  $('#form-personal').validate();
  //$('input.date').datepicker();
</script>