<p class="small-text"><?php echo get_phrase('add_expense');?></p>

        <?php echo form_open(base_url() . 'index.php?admin/accounting_expense/add/', array(
            'class' => 'form-horizontal form-groups-bordered validate expense-add','id'=>'form-personal','enctype' => 'multipart/form-data')); ?>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default required" aria-required="true">
            <label><?php echo get_phrase('title'); ?></label>
                <input type="text" class="form-control" name="title" required="" aria-required="true" placeholder="<?php echo get_phrase('title');?>">
                                    
            </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('category'); ?></label>
            <select name="expense_category_id" class="form-control selectboxit">
                <option><?php echo get_phrase('select_expense_category'); ?></option>
                <?php
                $categories = $this->db->get('expense_category')->result_array();
                foreach ($categories as $row):
                    ?>
                    <option value="<?php echo $row['expense_category_id']; ?>">
                        <?php echo $row['title']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default required" aria-required="true">
            <label><?php echo get_phrase('amount'); ?></label>
            <input type="text" class="form-control" name="amount" required="" aria-required="true" placeholder="<?php echo get_phrase('amount');?>" >
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('description'); ?></label>
            <input type="text" class="form-control" name="description"  placeholder="<?php echo get_phrase('description');?>">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('date'); ?></label>
            <input type="text" class="form-control date" name="timestamp"  placeholder="<?php echo get_phrase('date');?>">
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-4 col-sm-7">
        <button type="submit" class="btn btn-primary" id="submit-button"><?php echo get_phrase('add_expense');?></button>
     
     <span id="preloader-form"></span>
    </div>
</div>


<?php echo form_close(); ?>

<script>
    // url for refresh data after ajax form submission
    var post_refresh_url = '<?php echo base_url(); ?>index.php?admin/reload_expense_list';
</script>


<script type="text/javascript">

function validate_expense_add(formData, jqForm, options) {

    if (!jqForm[0].amount.value)
    {
        toastr.error("Please enter amount", "Error");
        return false;
    }
}

// ajax success response after form submission
function show_response_expense_add(responseText, statusText, xhr, $form)  {

    
    toastr.success("Expense added successfully", "Success");
    $('#modal_ajax').modal('hide');
    reload_data(post_refresh_url);
}



/*-----------------custom functions for ajax post data handling--------------------*/



// custom function for reloading table data
function reload_data(url)
{
    $.ajax({
        url: url,
        success: function(response)
        {
            // Replace new page data
            jQuery('.main_data').html(response);

        }
    });
}

</script>

<script type="text/javascript">  
  $('#form-personal').validate();
  $('input.date').datepicker({
    format: 'D,dd MM yyyy',
    
});
</script>
