<?php $edit_data	=	$this->db->get_where('account_role' , array('account_role_id' => $param2))->result_array();
foreach ($edit_data as $row):
?>
<div class="row">
	<div class="col-md-12">
		<div class="panel panel-primary" data-collapsed="0">
        	<div class="panel-heading">
            	<div class="panel-title" style="color:#626262">
            		<i class="fa fa-plus-circle"></i>
					<?php echo get_phrase('account_creation_form');?>
            	</div>
            </div>
			<div class="panel-body">
				
                <?php echo form_open(base_url() . 'index.php?admin/account_role/edit/'.$row['account_role_id'], array('class' => 'form-horizontal form-groups-bordered validate ajax-submit', 'enctype' => 'multipart/form-data' ,'id'=>'form-personal'));?>
	
					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('name');?></label>
                        
						<div class="col-sm-7">
                      	<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-user"></i></span>
								<input type="text" class="form-control" name="name" required data-validate="required" data-message-required="<?php echo get_phrase('value_required');?>" value="<?php echo $row['name'];?>" >
                         </div>
						</div>
					</div>
                    
					<hr />
                  <center>
                  	<span class="badge "><?php echo get_phrase('permissions');?></span>
                  </center><br>
                  <?php 
				  	$account_roles	=	$this->db->get('account_permission')->result_array();
					foreach ($account_roles as $row2):
					?>
					<div class="form-group">
						<label for="field-1" class="col-sm-5"><?php echo $row2['name'];?></label>
                        
						<div class="col-sm-6">
                      		    
						<label class="switch">
							<input type="checkbox" name="permission[]" value="<?php echo $row2['account_permission_id'];?>"
                            <?php if (in_array($row2['account_permission_id'] , explode(',' , $row['account_permissions']) ) )
										echo 'checked';?> >
							<div class="slider round"></div>
						</label>
						
						<i class="fa fa-info-circle popover-primary" style="margin-left:20px;" 
                         data-toggle="tooltip" title="<?php echo $row2['name'];?>">
                        </i>
           	
						</div>
					</div>
                  <?php
				  	endforeach;
					?>
                    
                    <div class="form-group">
						<div class="col-sm-offset-4 col-sm-7">
							<button type="submit" class="btn btn-primary" id="submit-button"><?php echo get_phrase('edit_account_role');?></button>
                         <span id="preloader-form"></span>
						</div>
					</div>
                <?php echo form_close();?>
            </div>
        </div>
    </div>
</div>
<?php endforeach;?>
<script>
	// url for refresh data after ajax form submission
	var post_refresh_url	=	'<?php echo base_url();?>index.php?admin/reload_account_role_list';
	var post_message		=	'Data Updated Successfully';
</script>


<!-- calling ajax form submission plugin for specific form -->
<script src="assets/js/ajax-form-submission.js"></script>

<script type="text/javascript">  
  $('#form-personal').validate();
  //$('input.date').datepicker();
</script>

<style>
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input {display:none;}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #6d5cae;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
<script>
    $('[data-toggle="tooltip"]').tooltip();   
</script>
<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
