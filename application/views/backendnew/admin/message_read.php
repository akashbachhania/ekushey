<?php
$messages         = $this->db->get_where('message' , array('message_thread_code' => $current_message_thread_code))->result_array();
foreach ($messages as $row):

  $sender         = explode('-' , $row['sender']);
  $sender_account_type  = $sender[0];
  $sender_id        = $sender[1];
  ?>



<div class="card share full-width">

    <div class="card-header clearfix">
      <div class="user-pic">
        <img src="<?php echo $this->crud_model->get_image_url($sender_account_type , $sender_id);?>" class="img-circle" width="33" height="33" data-src-retina="<?php echo $this->crud_model->get_image_url($sender_account_type , $sender_id);?>" data-src="<?php echo $this->crud_model->get_image_url($sender_account_type , $sender_id);?>">
      </div>
      <h5>
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> 
          <span><?php echo $this->db->get_where($sender_account_type , array($sender_account_type.'_id' => $sender_id))->row()->name;?></span>
        </a>
      </h5>
      <h6>
          <span class="location semi-bold"><?php echo date("d M, Y" , $row['timestamp']);?></span>
      </h6>
    </div>
    <div class="card-description">
      <p><?php echo $row['message'];?></p>

    </div>
  </div>

<?php endforeach;?>

<div class="card share full-width">
  
  <?php echo form_open(base_url() . 'index.php?admin/message/send_reply/' . $current_message_thread_code , array('enctype' => 'multipart/form-data'));?>
    <div class="mail-reply">
      <div class="">
        <textarea rows="4" class="form-control autogrow"  name="message" 
          placeholder="<?php echo get_phrase('reply_message');?>"></textarea>
      </div>
      <br>
      <button type="submit" class="btn btn-success btn-icon pull-right">
        <?php echo get_phrase('send');?>
        <i class="pg-mail"></i>
      </button>
      <br><br>
    </div>
  </form>

</div>