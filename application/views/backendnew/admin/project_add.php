
<br><br>
<div class="row">
<div class="col-md-2"></div>
    <div class="col-md-8">
        <div class="panel panel-primary" data-collapsed="0">
            <div class="panel-heading">
                <div class="panel-title" style="color:#626262" >
                    <i class="pg-plus"></i>
                    <?php echo get_phrase('project_form'); ?>
                </div>
            </div>
            <div class="panel-body">

                <?php echo form_open(base_url() . 'index.php?admin/project/create/', array('class' => 'form-horizontal form-groups-bordered validate', 'enctype' => 'multipart/form-data', 'id'=>'myform' )); ?>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 "><?php echo get_phrase('project_title'); ?></label>

                    <div class="col-sm-8">
                        <input type="text" class="form-control" name="title" id="title" required>
                    </div>
                </div>

                <div class="form-group">
                
                    <label for="field-1" class="col-sm-3"><?php echo get_phrase('description'); ?></label>
                    <!-- <div class="col-sm-12 wysiwyg5-wrapper email-body-wrapper">
                        <textarea class="wysiwyg email-body" rows="10" name="description" id="post_content"></textarea>
                    </div> -->
                
                    <div class="col-sm-8">
                        <textarea class="form-control wysihtml5" rows="10" name="description" id="post_content" 
                                  data-stylesheet-url="assets/css/wysihtml5-color.css"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 "><?php echo get_phrase('budget'); ?></label>

                    <div class="col-sm-5">
                        <div class="input-group">
                            <span class="input-group-addon primary"><i class="fa fa-align-justify"></i></span>
                            <input type="text" class="form-control" name="budget"  value="" >
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 "><?php echo get_phrase('start_time'); ?></label>

                    <div class="col-sm-5">
                        <div class="input-group">
                            <span class="input-group-addon primary"><i class="pg-calender"></i></span>
                            <input type="text" class="form-control date" name="timestamp_start"  value="" >
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 "><?php echo get_phrase('ending_time'); ?></label>

                    <div class="col-sm-5">
                        <div class="input-group">
                            <span class="input-group-addon primary"><i class="pg-calender"></i></span>
                            <input type="text" class="form-control date" name="timestamp_end"  value="" >
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 "><?php echo get_phrase('demo_url'); ?></label>

                    <div class="col-sm-5">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon-set globe-fill"></i></span>
                            <input type="text" class="form-control" name="demo_url"  value="" >
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 "><?php echo get_phrase('progress_status'); ?></label>

                    <div class="col-sm-5" style="padding-top:9px;">
                        <div class="slider2 slider slider-blue" data-prefix="" data-postfix="%" 
                             data-min="-1" data-max="101" data-value="0"></div>
                        <input type="hidden" name="progress_status" id="progress_status" value="0" >
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 "><?php echo get_phrase('client'); ?></label>

                    <div class="col-sm-5">
                    <select data-init-plugin="select2" name="client_id"   class="selectboxit full-width select2-offscreen" tabindex="-1" title="">
                            <option><?php echo get_phrase('select_a_client'); ?></option>
                            <?php
                            $clients = $this->db->get('client')->result_array();
                            foreach ($clients as $row):
                                ?>
                                <option value="<?php echo $row['client_id']; ?>">
                                    <?php echo $row['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 "><?php echo get_phrase('company'); ?></label>

                    <div class="col-sm-5">
                        <select name="company_id" class="form-control selectboxit">
                            <option><?php echo get_phrase('select_company'); ?></option>
                            <?php
                            $companies = $this->db->get('company')->result_array();
                            foreach ($companies as $row):
                                ?>
                                <option value="<?php echo $row['company_id']; ?>">
                                    <?php echo $row['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 "><?php echo get_phrase('assign_staff'); ?></label>

                    <div class="col-sm-8">
                        <select multiple="multiple" name="staffs[]" class="form-control multi-select">
                            <?php
                            $staffs = $this->db->get('staff')->result_array();
                            foreach ($staffs as $row):
                                ?>
                                <option value="<?php echo $row['staff_id']; ?>">
                                    <?php echo $row['name']; ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="field-1" class="col-sm-3 "></label>

                    <div class="col-sm-8">
                        <div class="checkbox check-primary  ">
                            <input type="checkbox" id="checkbox2" value="yes" checked="checked" name="notify_check">
                            <label for="checkbox2"><?php echo get_phrase('notify_client'); ?></label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-8">
                        <button type="submit" class="btn btn-primary" id="submit-button">
                            <?php echo get_phrase('add_new_project'); ?></button>
                        <span id="preloader-form"></span>
                    </div>
                </div>

                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
    <div class="col-md-2"></div>


</div>
<script src="<?php echo base_url()?>assets/pages/js/pages.email.js"></script>
<script src="<?php echo base_url()?>assets/assets/plugins/bootstrap3-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<script type="text/javascript">  
  $('#myform').validate();
  $('input.date').datepicker();
</script>



<link rel="stylesheet" href="assets/js/selectboxit/jquery.selectBoxIt.css">
<link rel="stylesheet" href="assets/js/wysihtml5/bootstrap-wysihtml5.css">
<!-- Bottom Scripts -->
<script src="assets/js/gsap/main-gsap.js"></script>
<script src="assets/js/bootstrap.js"></script>

<script src="assets/js/resizeable.js"></script>
<script src="assets/js/neon-api.js"></script>
<script src="assets/js/raphael-min.js"></script>
<script src="assets/js/morris.min.js"></script>

<script src="assets/js/wysihtml5/wysihtml5-0.4.0pre.min.js"></script>
<script src="assets/js/wysihtml5/bootstrap-wysihtml5.js"></script>
<script src="assets/js/neon-custom.js"></script>

<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script>



