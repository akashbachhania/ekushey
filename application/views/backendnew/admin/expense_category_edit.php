<?php 
    $edit_data = $this->db->get_where('expense_category' , array('expense_category_id' => $param2))->result_array();
    foreach ($edit_data as $row):
?>
<?php echo get_phrase('add_expense_category'); ?>
<?php echo form_open(base_url() . 'index.php?admin/accounting_expense_category/edit/' . $param2 , array(
   'id' => 'form-personal', 'class' => 'form-horizontal form-groups-bordered validate expense-category-edit', 'enctype' => 'multipart/form-data')); ?>
<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default required" aria-required="true">
            <label><?php echo get_phrase('title'); ?></label>
            <input type="text" required="" name="title" class="form-control" aria-required="true" placeholder="<?php echo get_phrase('title');?>" value="<?php echo $row['title'];?>" >
            </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12">
        <div pg-form-group class="form-group form-group-default">
            <label><?php echo get_phrase('description'); ?></label>
            <input type="text" class="form-control" name="description"  placeholder="<?php echo get_phrase('description');?>" value="<?php echo $row['description'];?>">
        </div>
    </div>
</div>
<div class="form-group">
    <div class="col-sm-offset-4 col-sm-7">
        <button type="submit" class="btn btn-primary" id="submit-button"><?php echo get_phrase('update'); ?></button>
        <span id="preloader-form"></span>
    </div>
</div>
<?php echo form_close(); ?>
<?php endforeach;?>


<script>
    // url for refresh data after ajax form submission
    var post_refresh_url = '<?php echo base_url(); ?>index.php?admin/reload_expense_category_list';
</script>

<script type="text/javascript">
    // ajax form plugin calls at each modal loading,
$(document).ready(function() {

   //config for project milestone adding
    var options = {
        beforeSubmit: validate_expense_category_edit,
        success: show_response_expense_category_edit,
        resetForm: true
    };
    $('.expense-category-edit').submit(function () {
        $(this).ajaxSubmit(options);
        return false;
    });
    
    
});

function validate_expense_category_edit(formData, jqForm, options) {

    if (!jqForm[0].title.value)
    {
        toastr.error("Please enter a title", "Error");
        return false;
    }
}

// ajax success response after form submission
function show_response_expense_category_edit(responseText, statusText, xhr, $form)  {

    
    toastr.success("Data updated successfully", "Success");
    $('#modal_ajax').modal('hide');
    reload_data(post_refresh_url);
}



/*-----------------custom functions for ajax post data handling--------------------*/



// custom function for reloading table data
function reload_data(url)
{
    $.ajax({
        url: url,
        success: function(response)
        {
            // Replace new page data
            jQuery('.main_data').html(response);

        }
    });
}

</script>
<script type="text/javascript">  
  $('#form-personal').validate();
  //$('input.date').datepicker();
</script>
