	<table class="table table-hover demo-table-dynamic" id="tableWithDynamicRows" >
        <thead>
          <tr>
          <th style="width:30px;"></th>
            <th><?php echo get_phrase('name');?></th>
            <th><?php echo get_phrase('permissions');?></th>
            <th><?php echo get_phrase('number_of_staff');?></th>
            <th><?php echo get_phrase('options');?></th>
          </tr>
        </thead>
        <tbody>
		<?php 
		$counter	=	1;
		$this->db->order_by('account_role_id' , 'desc');
		$account_roles	=	$this->db->get('account_role' )->result_array();
		foreach($account_roles as $row):
		?>
		<tr>
			<td class="v-align-middle" style="width:30px;">
           		<?php echo $counter++;?>
           	</td>
			<td class="v-align-middle">
				<?php echo $row['name'];?>
			</td>
			<td class="v-align-middle">
			<?php 
			$permission_array	=	( explode(',' , $row['account_permissions']));
			for($i = 0 ; $i<count($permission_array)-1; $i++)
			{
				echo '<span class="badge "> ';
				echo $this->db->get_where('account_permission',
						array('account_permission_id'=>$permission_array[$i]))->row()->name;
				echo "</span><br>";
			}
			?>

			</td>
			<td class="v-align-middle">
               <?php echo $this->db->get_where('staff',array('account_role_id'=>$row['account_role_id']))->num_rows();?>
           </td>
           <td class="v-align-middle">
            	<div class="btn-group">
                  <button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown">
                      Action <span class="caret"></span>
                  </button>
                  <ul class="dropdown-menu dropdown-default pull-right" role="menu">
                   <!-- EDITING LINK -->
                      <li>
                          <a href="#" onclick="showAjaxModal('<?php echo base_url();?>index.php?modal/popup/account_role_edit/<?php echo $row['account_role_id'];?>');">
                              <i class="fa fa-pencil"></i>
                                  <?php echo get_phrase('edit');?>
                              </a>
                                      </li>
                      <li class="divider"></li>
                      
                      <!-- DELETION LINK -->
                      <li>
                          <a href="#" onclick="confirm_modal('<?php echo base_url();?>index.php?admin/account_role/delete/<?php echo $row['account_role_id'];?>' , '<?php echo base_url();?>index.php?admin/reload_account_role_list');" >
                              <i class="fa fa-trash"></i>
                                  <?php echo get_phrase('delete');?>
                              </a>
                       </li>
                  </ul>
              </div>
			</td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>
<script src="assets/js/ajax-form-submission.js"></script>
                     
