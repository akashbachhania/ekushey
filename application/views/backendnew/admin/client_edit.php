<?php $edit_data	=	$this->db->get_where('client' , array('client_id' => $param2))->result_array();
foreach ($edit_data as $row):
?>
	<p class="small-text"><?php echo get_phrase('account_creation_form');?></p>		
				
                <?php echo form_open(base_url() . 'index.php?admin/client/edit/'.$row['client_id'], array('class' => 'form-horizontal form-groups-bordered validate ajax-submit', 'enctype' => 'multipart/form-data' , 'id'=>'form-personal'));?>
						
			          <div class="row">
            <div class="col-sm-12">
                <div pg-form-group class="form-group form-group-default required" aria-required="true">
                    <label><?php echo get_phrase('name'); ?></label>
                    <input type="text" required="" name="name" class="form-control" aria-required="true" placeholder="<?php echo get_phrase('name');?>" value="<?php echo $row['name'];?>" >
                    </div>
            </div>
        </div>
			        <div class="row">
			            <div class="col-sm-12">
			                <div pg-form-group class="form-group form-group-default">
			                    <label><?php echo get_phrase('email'); ?></label>
			                    <input type="text" class="form-control" name="email" value="<?php echo $row['email'];?>" >
			                </div>
			            </div>
			        </div>
			         <div class="row">
			            <div class="col-sm-12">
			                <div pg-form-group class="form-group form-group-default">
			                    <label><?php echo get_phrase('password'); ?></label>
			                    <input type="text" class="form-control" name="password"  >
			                </div>
			            </div>
			        </div>
			        <div class="row">
			            <div class="col-sm-12">
			                <div pg-form-group class="form-group form-group-default">
			                    <label><?php echo get_phrase('address'); ?></label>
			                    <input type="text" class="form-control" name="address" value="<?php echo $row['address'];?>" >
			                </div>
			            </div>
			        </div>
			        <div class="row">
			            <div class="col-sm-12">
			                <div pg-form-group class="form-group form-group-default">
			                    <label><?php echo get_phrase('phone'); ?></label>
			                    <input type="text" class="form-control" name="phone" value="<?php echo $row['phone'];?>" >
			                </div>
			            </div>
			        </div>
			        <div class="row">
			            <div class="col-sm-12">
			                <div pg-form-group class="form-group form-group-default">
			                    <label><?php echo get_phrase('website'); ?></label>
			                    <input type="text" class="form-control" name="website"  value="<?php echo $row['website'];?>">
			                </div>
			            </div>
			        </div>
			        <div class="row">
			            <div class="col-sm-12">
			                <div pg-form-group class="form-group form-group-default">
			                    <label><?php echo get_phrase('skype_id'); ?></label>
			                    <input type="text" class="form-control" name="skype_id" value="<?php echo $row['skype_id'];?>" >
			                </div>
			            </div>
			        </div>
			        <div class="row">
			            <div class="col-sm-12">
			                <div pg-form-group class="form-group form-group-default">
			                    <label><?php echo get_phrase('facebook_profile_link'); ?></label>
			                    <input type="text" class="form-control" name="facebook_profile_link"  value="<?php echo $row['facebook_profile_link'];?>">
			                </div>
			            </div>
			        </div>
			        <div class="row">
			            <div class="col-sm-12">
			                <div pg-form-group class="form-group form-group-default">
			                    <label><?php echo get_phrase('linkedin_profile_link'); ?></label>
			                    <input type="text" class="form-control" name="linkedin_profile_link"  value="<?php echo $row['linkedin_profile_link'];?>">
			                </div>
			            </div>
			        </div>
			        <div class="row">
			            <div class="col-sm-12">
			                <div pg-form-group class="form-group form-group-default">
			                    <label><?php echo get_phrase('twitter_profile_link'); ?></label>
			                    <input type="text" class="form-control" name="twitter_profile_link" value="<?php echo $row['twitter_profile_link'];?>" >
			                </div>
			            </div>
			        </div>
			        <div class="row">
			            <div class="col-sm-12">
			                <div pg-form-group class="form-group form-group-default">
			                    <label><?php echo get_phrase('short_note'); ?></label>
			                    <input type="text" class="form-control" name="short_note" value="<?php echo $row['short_note'];?>" >
			                </div>
			            </div>
			        </div>
			        <div class="row">
			            <div class="col-sm-12">
			                <div pg-form-group class="form-group form-group-default">
			                    <label><?php echo get_phrase('photo'); ?></label>
			                </div>
			            </div>
			        </div>

 

	
					<div class="form-group">
						<label for="field-1" class="col-sm-4 control-label"><?php echo get_phrase('photo');?></label>
                        
						<div class="col-sm-7">
							<div class="fileinput fileinput-new" data-provides="fileinput">
								<div class="fileinput-new thumbnail" style="width: 100px; height: 100px;" data-trigger="fileinput">
									<img src="<?php echo $this->crud_model->get_image_url('client' , $row['client_id']);?>" alt="...">
								</div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px"></div>
								<div>
									<span class="btn btn-white btn-file">
										<span class="fileinput-new">Select image</span>
										<span class="fileinput-exists">Change</span>
										<input type="file" name="userfile" accept="image/*">
									</span>
									<a href="#" class="btn btn-orange fileinput-exists" data-dismiss="fileinput">Remove</a>
								</div>
							</div>
						</div>
					</div>
                    
                    <div class="form-group">
						<div class="col-sm-offset-4 col-sm-7">
							<button type="submit" class="btn btn-primary" id="submit-button"><?php echo get_phrase('edit_client');?></button>
                         <span id="preloader-form"></span>
						</div>
					</div>
                <?php echo form_close();?>
            
<?php endforeach;?>
<script>
	// url for refresh data after ajax form submission
	var post_refresh_url	=	'<?php echo base_url();?>index.php?admin/reload_client_list';
	var post_message		=	'Data Updated Successfully';
</script>

<!-- calling ajax form submission plugin for specific form -->
<script src="assets/js/ajax-form-submission.js"></script>

<script type="text/javascript">  
  $('#form-personal').validate();
  //$('input.date').datepicker();
</script>