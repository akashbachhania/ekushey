      <!-- START HEADER -->
      <div class="header ">
       <?php //echo $system_name;?>
        <div class=" pull-left sm-table hidden-xs hidden-sm">
          <div class="header-inner">
            <div class="brand inline">
                <img width="78" height="22" data-src-retina="<?php echo base_url()?>assets/assets/img/logo_2x.png" data-src="<?php echo base_url()?>assets/assets/img/logo.png" alt="logo" src="<?php echo base_url()?>assets/assets/img/logo.png">
            </div>
            
            <!-- START NOTIFICATION LIST -->
            <ul class="notification-list no-margin hidden-sm hidden-xs b-grey b-l b-r no-style p-l-30 p-r-20">
              <?php
                $total_unread_message_number        =   0;
                $current_user                       =   $this->session->userdata('login_type').'-'.$this->session->userdata('login_user_id');

                $this->db->where('sender' , $current_user);
                $this->db->or_where('reciever' , $current_user);
                $message_threads                    =   $this->db->get('message_thread')->result_array();
                foreach($message_threads as $row) {
                    $unread_message_number          =   $this->crud_model->count_unread_message_of_thread($row['message_thread_code']);
                    $total_unread_message_number    +=  $unread_message_number;
                }
                ?>
              <li class="p-r-15 inline">
                <div class="dropdown">
                  <a href="javascript:;" id="notification-center" class="pg-home" data-toggle="dropdown">
                  <?php if ($total_unread_message_number >0 ):?>
                    <span class="bubble">&nbsp;<?php echo $total_unread_message_number;?></span>
                    <?php endif;?>
                  </a>
                  <!-- START Notification Dropdown -->
                  <div class="dropdown-menu notification-toggle" role="menu" aria-labelledby="notification-center">
                    <!-- START Notification -->
                    <div class="notification-panel">
                      <!-- START Notification Body-->
                      <div class="notification-body scrollable">

                          <?php
                          $current_user               =   $this->session->userdata('login_type').'-'.$this->session->userdata('login_user_id');
                          $this->db->where('sender' , $current_user);
                          $this->db->or_where('reciever' , $current_user);
                          $message_threads            =   $this->db->get('message_thread')->result_array();
                          foreach($message_threads as $row):

                              // defining the user to show
                              if ($row['sender'] == $current_user)
                                  $user_to_show       =   explode('-' , $row['reciever']);
                              if ($row['reciever'] == $current_user)
                                  $user_to_show       =   explode('-' , $row['sender']);
                              $user_to_show_type      =   $user_to_show[0];
                              $user_to_show_id        =   $user_to_show[1];
                              $unread_message_number  =   $this->crud_model->count_unread_message_of_thread($row['message_thread_code']);
                              if ($unread_message_number == 0)
                                  continue;

                              // the last sent message from the opponent user
                              $this->db->order_by('timestamp' , 'desc');
                              $last_message_row       =   $this->db->get_where('message',array('message_thread_code' => $row['message_thread_code']) )->row();
                              $last_unread_message    =   $last_message_row->message;
                              $last_message_timestamp =   $last_message_row->timestamp;

                          ?>
                        <!-- START Notification Item-->
                        <div class="notification-item  clearfix">
                          <div class="heading">
                            <a href="<?php echo base_url();?>index.php?<?php echo $this->session->userdata('login_type');?>/message/message_read/<?php echo $row['message_thread_code'];?>" class="text-danger pull-left">
                              <span class="bold">
                                <strong>
                                  <?php echo $this->db->get_where($user_to_show_type , array($user_to_show_type.'_id' => $user_to_show_id))->row()->name;?>
                                </strong>
                            

                              </span>
                              <span class="fs-12 m-l-10"><?php echo substr($last_unread_message , 0 , 50);?></span>
                            </a>
                            <span class="pull-right time">
                            - <?php echo date("d M, Y" , $last_message_timestamp);?>
                            <img src="<?php echo $this->crud_model->get_image_url($user_to_show_type , $user_to_show_id);?>" height="38" class="img-circle" />
  
                            </span>

                          </div>
                          <!-- START Notification Item Right Side-->
                          <div class="option">
                            <a href="<?php echo base_url();?>index.php?<?php echo $this->session->userdata('login_type');?>/message/message_read/<?php echo $row['message_thread_code'];?>" class="mark"></a>
                          </div>
                          <!-- END Notification Item Right Side-->
                        </div>
                        <?php endforeach;?>




                        <!-- END Notification Item-->
                       </div>
                      <!-- END Notification Body-->
                      <!-- START Notification Footer-->
                      <div class="notification-footer text-center">
                        <a href="<?php echo base_url();?>index.php?<?php echo $this->session->userdata('login_type');?>/message" class="">
                        <?php echo get_phrase('view_all_messages');?></a>
                        <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="<?php echo base_url();?>index.php?<?php echo $this->session->userdata('login_type');?>/message">
                          <i class="pg-refresh_new"></i>
                        </a>
                      </div>
                      <!-- START Notification Footer-->
                    </div>
                    <!-- END Notification -->
                  </div>
                  <!-- END Notification Dropdown -->
                </div>
              </li>
            <!-- Project status // shown to admin only-->
            <?php 
                if ($this->session->userdata('login_type') == 'admin'):

                $this->db->where('progress_status <' , 100);
                $this->db->where('project_status' , 1);
                $this->db->order_by('project_id' , 'desc');
                $projects   =   $this->db->get('project' )->result_array();
                ?>
            
              <li class="p-r-15 inline">
                <div class="dropdown">
                  <a href="javascript:;" id="notification-center" class="fa fa-paper-plane" data-toggle="dropdown">
                    <?php if (count($projects) > 0):?>
                    <span class="bubble">&nbsp;<?php echo count($projects);?></span>
                    <?php endif;?>
                  </a>
                  <!-- START Notification Dropdown -->
                  <div class="dropdown-menu notification-toggle" role="menu" aria-labelledby="notification-center">
                    <!-- START Notification -->
                    <div class="notification-panel">
                      <!-- START Notification Header-->
                      <div class="notification-footer text-center">
                         <strong><?php echo get_phrase('running_projects');?></strong>
                       </div>
                      <!-- START Notification Header-->
                      <!-- START Notification Body-->
                      <div class="notification-body scrollable">
                    <?php 
                    foreach($projects as $row):
                        $status = 'info';
                        if ($row['progress_status'] == 100)$status = 'success';
                        if ($row['progress_status'] < 50)$status = 'danger';
                        ?>
                        <!-- START Notification Item-->
                        <div class="notification-item  clearfix">
                          <div class="heading">
                            <a href="<?php echo base_url();?>index.php?admin/projectroom/wall/<?php echo $row['project_code'];?>" class="text-danger pull-left">
                              <i class="fa fa-exclamation-triangle m-r-10"></i>
                              <span class="bold"><?php echo $row['title'];?></span>
                              <span class="fs-12 m-l-10"><?php echo $row['progress_status'];?>%</span>
                            </a>
<!--                             <span class="pull-right time"><?php //echo $row['progress_status'];?>% Complete</span>
 -->                          </div>
                          <!-- START Notification Item Right Side-->
                          <div class="option">
                            <a href="#" class="mark"></a>
                          </div>
                          <!-- END Notification Item Right Side-->
                        </div>
                      <?php endforeach;?>  
                        <!-- END Notification Item-->
                       </div>
                      <!-- END Notification Body-->
                      <!-- START Notification Footer-->
                      <div class="notification-footer text-center">
                        <a href="<?php echo base_url();?>index.php?admin/project" class="">
                         <?php echo get_phrase('view_all_projects');?>
                        </a>
                        <a data-toggle="refresh" class="portlet-refresh text-black pull-right" href="<?php echo base_url();?>index.php?<?php echo $this->session->userdata('login_type');?>/message">
                          <i class="pg-refresh_new"></i>
                        </a>
                      </div>
                      <!-- START Notification Footer-->
                    </div>
                    <!-- END Notification -->
                  </div>
                  <!-- END Notification Dropdown -->
                </div>
              </li>
              <?php endif;?>
              <li class="p-r-40 inline">
              <h4 style="font-weight:200; margin:0px;"><?php echo $system_name;?></h4>
              </li>

            </ul>
            <!-- END NOTIFICATIONS LIST -->
           </div>

        </div>
        <div class=" pull-right">
          <div class="header-inner">
            <a href="#" class="btn-link icon-set menu-hambuger-plus m-l-20 sm-no-margin hidden-sm hidden-xs" data-toggle="quickview" data-toggle-element="#quickview"></a>
          </div>
        </div>
        <div class=" pull-right">
          <!-- START User Info-->
          <div class="visible-lg visible-md m-t-10">
            <div class="pull-left p-r-10 p-t-10 fs-16 font-heading">
            <!--   <span class="semi-bold">David</span> <span class="text-master">Nest</span>
             --></div>
            <div class="dropdown pull-right">
              <button class="profile-dropdown-toggle " type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="thumbnail-wrapper d32 circular inline m-t-5 ">
                <i class="glyphicon glyphicon-user"></i>
            </span>
              </button>
              <ul class="dropdown-menu profile-dropdown " role="menu">
                <li><a href="<?php echo base_url();?>index.php?<?php echo $this->session->userdata('login_type');?>/manage_profile"><i class="pg-settings_small"></i>Edit Profile</a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>index.php?<?php echo $this->session->userdata('login_type');?>/manage_profile">
                    <i class="pg-outdent"></i> Change Password</a>
                </li>
                <!-- <li><a href="#"><i class="pg-signals"></i> Help</a>
                </li>
                 -->
                 <li class="bg-master-lighter">
                  <a href="<?php echo base_url();?>index.php?login/logout" class="clearfix">
                    <span class="pull-left">Logout</span>
                    <span class="pull-right"><i class="pg-power"></i></span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <!-- END User Info-->
        </div>
      </div>
      <!-- END HEADER -->